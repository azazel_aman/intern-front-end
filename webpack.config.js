const path = require("path");
const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
// const ExtractTextPlugin = require('extract-text-webpack-plugin');

const webpack = require("webpack")
module.exports = {
  mode: 'development',

  entry: {
    index: path.resolve(__dirname, "src/js/index.js"),
    admin: path.resolve(__dirname, "src/js/admin.js"),
    sign_up: path.resolve(__dirname, "src/js/sign_up.js"),
    dashboard1: path.resolve(__dirname, "src/js/dashboard1.js"),
    switch: path.resolve(__dirname,"src/js/switch.js"),
    editProfile: path.resolve(__dirname,"src/js/editProfile.js")
  },

  output: {
    path: path.resolve(__dirname, "static"),
    filename: "js/[name].[hash].js"
  },

  plugins: [
    new HtmlWebpackPlugin({
      filename: path.resolve(__dirname, "templates", "index.html"),
      template: path.resolve(__dirname, "src", "html", "index.html"),
      chunks: ['index']
    }),

    new HtmlWebpackPlugin({
      filename: path.resolve(__dirname, "templates", "admin.html"),
      template: path.resolve(__dirname, "src", "html", "admin.html"),
      chunks: ['admin']
    }),

    new HtmlWebpackPlugin({
      filename: path.resolve(__dirname, "templates", "sign_up.html"),
      template: path.resolve(__dirname, "src", "html", "sign_up.html"),
      chunks: ['sign_up']
    }),
    new HtmlWebpackPlugin({
      filename: path.resolve(__dirname, "templates", "dashboard1.html"),
      template: path.resolve(__dirname, "src", "html", "dashboard1.html"),
      chunks: ['dashboard1']
    }),
    new HtmlWebpackPlugin({
      filename: path.resolve(__dirname, "templates", "switch.html"),
      template: path.resolve(__dirname, "src", "html", "switch.html"),
      chunks: ['switch']
    }),
    new HtmlWebpackPlugin({
      filename: path.resolve(__dirname, "templates", "editProfile.html"),
      template: path.resolve(__dirname, "src", "html", "editProfile.html"),
      chunks: ['editProfile']
    }),
    new CleanWebpackPlugin(["static/js/*.js", "static/css/*.css", "templates/*.html","static/css/images/*","static/css/webfonts/*"]),

    new MiniCssExtractPlugin({
      filename: "./css/[name].[hash].css"
    }),
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery'
    }),

  ],
  module: {

    rules: [
    {
      test: /\.(svg|png|jpg|jpeg)$/,
      use: {
        loader: "file-loader",
        options:{
          name: "[name][hash].[ext]",
          publicPath: './images',
          outputPath: './css/images'
        }
      }
    },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader"
        }
      },
      {
        test: /\.(css|sass|scss)$/,
        use: [
          MiniCssExtractPlugin.loader, "css-loader", "postcss-loader", "sass-loader"
        ]
      },
      {
        test: /\.(woff|woff2|ttf|eot|otf|svg)$/,
        use: {
          loader: "file-loader",
          options: {
            name: "[name][hash].[ext]",
            publicPath: './webfonts',
            outputPath: './css/webfonts'
          }
        }
      }
    ]
  }
}
