import '../sass/editProfile.sass'
// import '../sass/template.sass'
import 'jquery'
import '../font-awesome/fontawesome.css'
import * as Error from './error'
var token=localStorage.getItem("token");
var Heirarchy = ['Basic', 'Option'];
var host = "https://kn-career-matrix.herokuapp.com";
var currentTop = 'UserDetails';
var emp_id=localStorage.getItem("emp_id");
var currentStatus =
{
    currentBasic:
    {
        "name": undefined,
        "kn_code": undefined,
        "password": undefined,
        "password_confirmation": undefined
    },
    currentCountry:
    {
        type: "country",
        id: undefined
    },
    currentFramework:
    {
        type: "framework",
        id: undefined
    },
    currentLevel:
    {
        type: "level",
        id: undefined
    }
};
var inputFields =
{
    "Basic": [
    {
        "placeholder": "Full Name",
        "name": "Name",
        "type": "text"
    },
    {
        "placeholder": "Email Address",
        "name": "email",
        "type": "email"
    },
    {
        "placeholder": "KN Code",
        "name": "kNCode",
        "type": "text"
    },
    {
        "placeholder": "Password",
        "name": "password",
        "type": "password",
    },
    {
        "placeholder": "Confirm Password",
        "name": "password_confirmation",
        "type": "password"
    }],
    "Level": [
        {
            "placeholder": "Label",
            "name": "label",
            "type": "text"
        },
        {
            "placeholder": "Threshold XP",
            "name": "threshold_xp",
            "type": "number"
        }
    ],
    "UserDetails": [
    {
        "placeholder": "name"
    }],
    "Country": [{}]

};
var dataTypes = {
    "text": "string",
    "number": "integer",
    "checkbox": "boolean",
    "textarea": "string"
}

$("body").on("click", ".sidebar-title", function() {
    let order = currentTop;
    var destPane;
    var strSlice = $(this).html().trim();
    if (strSlice != currentTop) {
        var requiredId = "#add" + strSlice;
        for (let i = 3; i >= 0; i--) {
            destPane = '#add' + Heirarchy[i];
            if (requiredId == destPane) {
                currentTop = Heirarchy[i];
                break;
            } else {
                $(destPane).css("right", "-100vw");
            }
        }
    }
});

function comparePassword()
{
	if($("#password").val().trim()=="")
	{
		return true
	}
	else
	{
		if ($("#password").val().trim() == $("#password_confirmation").val().trim() && $("#password_confirmation").val().trim().length>=8)
		{
		    return true
		}
		if($("#password_confirmation").val().length<8)
		{
		    $("#error-password_confirmation").html("The password should be a minimum of 8 characters");
		}
		else
		{
		    $("#error-password_confirmation").html("Password does not match");
		}
		return false	
	}
    
}

$("*").dblclick(function(e)
{
    e.preventDefault();
});
$("body").on("click", "#addNewOption", function()
{
    clearError();
    
        if (comparePassword() == true)
        {
            currentStatus.currentBasic.name = $("input#name").val();
            currentStatus.currentBasic.kn_code = $("input#kn_code").val();
            currentStatus.currentBasic.password = $("input#password").val();
            currentStatus.currentBasic.password_confirmation = $("input#password_confirmation").val();
            if(currentStatus.currentBasic.name=="")
            {
                delete currentStatus.currentBasic.name;   
            }
            if(currentStatus.currentBasic.kn_code=="")
            {
                delete currentStatus.currentBasic.kn_code;
            }
            if(currentStatus.currentBasic.password =="")
            {
                delete currentStatus.currentBasic.password ;
                delete currentStatus.currentBasic.password_confirmation;
            }
            currentTop = 'Option';
            let settings =
            {
                "async": true,
                "crossDomain": true,
                "url": "https://kn-career-matrix.herokuapp.com/api/v1/employees/" + emp_id,
                "method": "PATCH",
                "headers":
                {
                    "Authorization": "Bearer " + token,
                    "Content-Type": "application/json"
                },
                "data": JSON.stringify(
                {
                    "data": {
                        "type": "employee",
                        "id": emp_id,
                        "attributes": currentStatus.currentBasic
                    }
                }),
                success: function(data)
                {
                	$("#name").val(localStorage.setItem("name",$("#name").val()));
                	$("#kn_code").val(localStorage.setItem("kn_code",$("#kn_code").val()));
                    window.location.href="./dashboard"
                },
                error: function(error)
                {
                    Error.globalError(error.responseJSON.errors[0].detail);
                    console.log(error);
                }
            }
console.log(settings);
            $.ajax(settings);
        }
    
});

$(document).ready(function()
{
$(".loader").hide();
            $(".siteContent").css("display","flex");
            let name=localStorage.getItem("name");
            let code=localStorage.getItem("kn_code");
            $("input[type='text']#name").val(name);
            $("input[type='text']#kn_code").val(code);
         
            
});

function basicValidate() {
    var listOfInputs = $("#addBasic").find("input");
    for (let i = 0; i < listOfInputs.length; i++)
    {
        if (listOfInputs[i].value == "")
        {
            listOfInputs[i].parentElement.nextElementSibling.innerHTML = "Empty Field"
            return false;
        }
    }
    return true;
}

function clearError()
{
    $(".error").html("");
}

$("input").keypress(function(e){
if(e.keyCode==13)
{
    e.preventDefault();
    document.getElementsByTagName("button")[0].click();
}
})

function setBackground()
{
    let background=[`https://images.unsplash.com/photo-1500835556837-99ac94a94552`,
                    `https://images.unsplash.com/photo-1470922792794-b15b12d20e80`,
                    `https://images.unsplash.com/photo-1519213887655-a4f199e3015b`,
                    `https://images.unsplash.com/photo-1476778642660-a2c55571cf0d`,
                    `https://images.unsplash.com/photo-1465844880937-7c02addc633b`]
    let numOfSidebar=$(".sidebar").length;

    for(let i=0;i<numOfSidebar;i++)
    {
        let num=(Math.floor(Math.random()*100))%4;
        $(".sidebar")[i].style.backgroundImage="url("+background[num]+")";
    }
}
$("#level-up-option").on("click",function(){
    levelup();
});
$("#signOut-option").on("click",function(){
    localStorage.clear()
    window.location.href="./"
});
$("#admin-option").on("click",function(){
    
    window.location.href="./admin"
});
