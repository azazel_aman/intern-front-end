import 'jquery'
import '../sass/admin.sass'
// import '../sass/template.sass'
import '../font-awesome/fontawesome.css';
import * as Error from './error'
// import '../sass/navbar_lip.sass'
// import '../sass/loader.sass'
// import '../sass/error.sass'
// import '../sass/media.sass'
// import '../sass/input.sass'

// import '../sass/sidebar.sass'
// import '../sass/scrollbar.sass'
// import '../sass/breadcrumb.sass'
//list of global variables
var country_code = 1;
var Heirarchy = ['Framework', 'Level', 'Section', 'Objective'];
var token = "";
var host = "https://kn-career-matrix.herokuapp.com";
var currentTop = 'Framework'
var currentStatus = {
    currentFramework: {
        label: undefined,
        id: undefined
    },
    currentLevel: {
        label: undefined,
        id: undefined,
        threshold_xp: undefined
    },
    currentSection: {
        label: undefined,
        id: undefined
    },
    currentObjective: {
        id: undefined,
        title: undefined,
        description: undefined,
        xp: undefined,
        essential: undefined
    }
};
var inputFields = {
    "Framework": [{
        "placeholder": "Label",
        "name": "label",
        "type": "text"
    }],
    "Level": [{
            "placeholder": "Label",
            "name": "label",
            "type": "text"
        },
        {
            "placeholder": "Threshold XP",
            "name": "threshold_xp",
            "type": "number"
        }
    ],
    "Section": [{
        "placeholder": "Label",
        "name": "label",
        "type": "text"
    }],
    "Objective": [{
            "placeholder": "Title",
            "name": "title",
            "type": "text"
        },
        {
            "placeholder": "Description",
            "name": "description",
            "type": "textarea"
        },

        {
            "placeholder": "XP",
            "name": "xp",
            "type": "number"
        },
        {
            "placeholder": "Essential",
            "name": "essential",
            "type": "checkbox"
        }
    ]
};
var dataTypes = {
    "text": "string",
    "number": "integer",
    "checkbox": "boolean",
    "textarea": "string"
}
var Delete_id;
var Delete_order;
 $("body").on("click", ".confirm", function()
    {
        $(".siteContent").off("click");
        if (Delete_order == "Framework")
        {
            ajaxShow(Delete_order, "DELETE", "/api/v1/countries/1/frameworks/" + Delete_id, { "Content-Type": "application/json", Authorization: "Bearer " + token }, {}, refreshLeftColumn);
        }
        else if (Delete_order == "Level")
        {
            ajaxShow(Delete_order, "DELETE", "/api/v1/frameworks/" + currentStatus.currentFramework.id + "/levels/" + Delete_id, { "Content-Type": "application/json", Authorization: "Bearer " + token }, {}, refreshLeftColumn);
        }
        else if (Delete_order == "Section")
        {
            ajaxShow(Delete_order, "DELETE", "/api/v1/levels/" + currentStatus.currentLevel.id + "/sections/" + Delete_id, { "Content-Type": "application/json", Authorization: "Bearer " + token }, {}, refreshLeftColumn);
        }
        else if (Delete_order == "Objective")
        {
            ajaxShow(Delete_order, "DELETE", "/api/v1/" + "sections/" + currentStatus.currentSection.id + "/objectives/" + Delete_id, { "Content-Type": "application/json", Authorization: "Bearer " + token }, {}, refreshLeftColumn);
        }
        $(".globalWarningConfirm").css("bottom", "-3.125rem");
        $(".globalWarningConfirm").removeClass('invisible').removeClass('display');
        $(".globalWarningConfirm").hide();
        

    });

function clearStatus(order)
{
    var list = Object.keys(currentStatus["current" + order]);
    for (let i = 0; i < list.length; i++) {
        currentStatus["current" + order][list[i]] = undefined;
    }
}
//Back traversing with breadcrumbs
$("body").on("click", ".singleBC", function()
 {
    if ($(this).hasClass('current'))
    {
        return null;
    }
    let order = currentTop;
    var destPane;
    var strSlice = $(this).attr("id").slice(5);
    var requiredId = "#add" + strSlice;
    for (let i = 3; i >= 0; i--)
    {
        discard(order);
        order = Heirarchy[i];
        destPane = '#add' + Heirarchy[i];
        if (requiredId == destPane)
        {
            currentTop = Heirarchy[i];
            break;
        } else
        {
            clearStatus(Heirarchy[i]);
            $(destPane).css({
                "-webkit-transform": "translateX(100vw)",
                "-ms-transform": "translateX(100vw)",
                "transform": "translateX(100vw)"
            });
        }
    }
});


//sidebar clicking
$("body").on("click", ".sidebar-title", function()
{
    let order = currentTop;
    var destPane;
    var strSlice = $(this).html().trim();
    if (strSlice != currentTop)
    {
        discard(order);
        var requiredId = "#add" + strSlice;
        for (let i = 3; i >= 0; i--)
        {
          discard(order);
          destPane = '#add' + Heirarchy[i];
          if (requiredId == destPane)
          {
            currentTop = Heirarchy[i];
            break;
          } else
          {
            clearStatus(Heirarchy[i]);
            $(destPane).css({
                "-webkit-transform": "translateX(100vw)",
                "-ms-transform": "translateX(100vw)",
                "transform": "translateX(100vw)"
            });
          }
        }
    }
});


//Setting BreadCrumbs
function setBreadCrumbs()
{
    $("#crumbFramework .content").html(currentStatus.currentFramework.label);
    $("#crumbLevel .content").html(currentStatus.currentLevel.label);
    $("#crumbSection .content").html(currentStatus.currentSection.label);
    $("#crumbObjective .content").html("Manage Objectives");
}

//onLoad function
$("*").dblclick(function(e)
{
    e.preventDefault();
});

$(document).ready(function()
{   
    $("#name").html(localStorage.getItem("name"));
    $("#kn_code").html(localStorage.getItem("kn_code"));
    $("#accumulated_xp").html(localStorage.getItem("total_xp"));
    token=localStorage.getItem("token");
    setToken(ajaxShow);
    browserWidth();
    $(".siteContent").removeClass("invisible").addClass("display");
    $(".loader").removeClass('display').addClass('invisible');
});
//set token
function setToken(callback)
{
            callback("Framework", "GET", "/api/v1/countries/1/frameworks", { Authorization: "Bearer " + token }, {});
}

//ajax request related to framework

function ajaxShow(order, method, url, header, data, callback)
{
    var settings = {
        beforeSend: function()
        {
            $('.loader').removeClass("invisible").addClass("display");
        },
        complete: function()
        {
            $('.loader').removeClass("display").addClass("invisible");
        },
        "async": true,
        "crossDomain": true,
        "url": host + url,
        "method": method,
        "headers": header,
        "data": JSON.stringify(data),
        success: function(data)
        {
            backToCreate(order);
            $(".error").html("");
            if (method == "GET") {
                if (order != "Objective")
                {
                    $("#present" + order).html("");
                    for (let i = 0; i < data.data.length; i++)
                    {
                        $("#present" + order).prepend(`<div data = '` + JSON.stringify(data.data[i].attributes) + `' class="list" id="` + data.data[i].id + `"> <div class="listLabel">` + data.data[i].attributes.label + `</div> <div class="iconContainer"> <div class="edit"><i class="fas fa-edit"></i></div> <div class="delete"> <i class="fas fa-trash-alt"></i> </div> </div> </div>`);
                    }
                }
                else
                {
                    $("#present" + order).html("");
                    for (let i = 0; i < data.data.length; i++)
                    {
                        $("#present" + order).prepend(`<div data = '` + JSON.stringify(data.data[i].attributes) + `' class="list" id="` + data.data[i].id + `"> <div class="listLabel">` + data.data[i].attributes.title + `</div> <div class="iconContainer"> <div class="edit"> <i class="fas fa-edit"></i> </div> <div class="delete"> <i class="fas fa-trash-alt"></i> </div> </div> </div>`)
                    }
                }
            }
            else if (method == "DELETE")
            {
                callback(order);
                Error.globalSuccess("Successfully Deleted");
                $(".siteContent").removeClass('freeze');
            }
            else if (method == "POST")
            {
                if (order == "Objective")
                {
                    currentStatus["current" + order].id = data.data.id;
                    currentStatus["current" + order].title = data.data.attributes.title;
                    Error.globalSuccess(currentStatus["current" + order].title + " Successfully Appended");
                    callback(order);
                }
                else
                {
                    currentStatus["current" + order].id = data.data.id;
                    currentStatus["current" + order].label = data.data.attributes.label;
                    Error.globalSuccess(currentStatus["current" + order].label + " Successfully Appended");
                    callback(order);
                }
                $("body").removeClass("freeze");
                $("#add" + order).find("input:not([type='checkbox'])").val("");
            }
            else
            {
                discard(order);
                backToCreate(order);
                callback(order);
                Error.globalSuccess("Successfully Updated");
            }
        },
        error: function(error)
        {
            console.log(error);
            Error.globalError(error.responseJSON.errors[0].detail);
        }
    }
    if (settings.method == "GET" || settings.method == "DELETE")
    {
        delete settings["data"];
    }

    $.ajax(settings);
}

//sidebar draweer in
$("body").on("click", "#addNewLevel", function()
{
    discard(currentTop);
    ajaxShow("Level", "GET", "/api/v1/frameworks/" + currentStatus.currentFramework.id + "/levels", { "Content-Type": "application/json", Authorization: "Bearer " + token }, "");
    $("#addLevel").css({
                "-webkit-transform": "translateX(-100vw)",
                "-ms-transform": "translateX(-100vw)",
                "transform": "translateX(-100vw)"
            });
    currentTop = 'Level';
    setBreadCrumbs()
});

$("body").on("click", "#addNewObjective", function()
{
    discard(currentTop);
    ajaxShow("Objective", "GET", "/api/v1/sections/" + currentStatus.currentSection.id + "/objectives", { "Content-Type": "application/json", Authorization: "Bearer " + token }, "");
    $("#addObjective").css({
                "-webkit-transform": "translateX(-100vw)",
                "-ms-transform": "translateX(-100vw)",
                "transform": "translateX(-100vw)"
            });
    currentTop = 'Objective';
    setBreadCrumbs()
});

$("body").on("click", "#addNewSection", function()
{
    discard(currentTop);
    ajaxShow("Section", "GET", "/api/v1/levels/" + currentStatus.currentLevel.id + "/sections", { "Content-Type": "application/json", Authorization: "Bearer " + token }, "");
    $("#addSection").css({
                "-webkit-transform": "translateX(-100vw)",
                "-ms-transform": "translateX(-100vw)",
                "transform": "translateX(-100vw)"
            });
    currentTop = 'Section';
    setBreadCrumbs()
});

//manupilating the list of present state
$("body").on("click", ".save", function()
{
    let order = currentTop;
    $("#add" + order).find(".discard").html("Discard");
    var loop = inputFields[order].length;
    var parameter = { data: { type: order.toLowerCase(), attributes: {} } };
    var a;
    for (let i = 0; i < loop; i++)
    {
        a = inputFields[order][i];
        if (a.type == "checkbox")
        {
            var valueOfCheckBox = $("body").find("#" + order + "-" + a.name)[0].checked;
            if (valueOfCheckBox == true || valueOfCheckBox == false)
            {
                currentStatus["current" + order][a.name] = valueOfCheckBox;
            }
        }
        else if (dataTypes[a.type] == "integer")
        {
            currentStatus["current" + order][a.name] = parseInt($("body").find("#" + order + "-" + a.name)[0].value);
        } else
        {
            currentStatus["current" + order][a.name] = $("body").find("#" + order + "-" + a.name)[0].value;
        }
    }
    parameter.data.attributes = JSON.parse(JSON.stringify((currentStatus["current" + order])));
    delete parameter.data.attributes["id"];

    if (emptyParameter(parameter) == "Empty Field")
    {
        // globalError("Empty Field");
    }
    else if (emptyParameter(parameter) == false)
    {
        // console.log("Incorrect value format");
        Error.globalError("Incorrect value format");
    }
    else
    {
        if (order == "Framework")
        {
            ajaxShow("Framework", "POST", "/api/v1/countries/1/frameworks", { "Content-Type": "application/json", Authorization: "Bearer " + token }, parameter, refreshLeftColumn);
            backToCreate("Framework");
        }
        else if (order == "Level")
        {
            ajaxShow("Level", "POST", "/api/v1/frameworks/" + currentStatus.currentFramework.id + "/levels", { "Content-Type": "application/json", Authorization: "Bearer " + token }, parameter, refreshLeftColumn);
            backToCreate("Level");
        }
        else if (order == "Section")
        {
            ajaxShow("Section", "POST", "/api/v1/levels/" + currentStatus.currentLevel.id + "/sections", { "Content-Type": "application/json", Authorization: "Bearer " + token }, parameter, refreshLeftColumn);
            backToCreate("Section");
        }
        else if (order = "Objective")
        {
            ajaxShow("Objective", "POST", "/api/v1/sections/" + currentStatus.currentSection.id + "/objectives", { "Content-Type": "application/json", Authorization: "Bearer " + token }, parameter, refreshLeftColumn);
        }
    }
});

$("body").on("click", ".update", function()
{
    let order = currentTop;
    var loop = inputFields[order].length;
    var parameter = { data: { type: currentTop.toLowerCase(), attributes: {} } };
    var a = $("#add" + order).find("input , textarea");
    for (let i = 0; i < loop; i++)
    {
        if (inputFields[order][i].type == "checkbox")
        {
            currentStatus["current" + order][a[i].id.slice(order.length + 1)] = a[i].checked;
        }
        else
        {
            currentStatus["current" + order][a[i].id.slice(order.length + 1)] = a[i].value;
        }
    }
    parameter.data.attributes = JSON.parse(JSON.stringify((currentStatus["current" + order])));
    parameter.data.id = currentStatus["current" + order].id;
    delete parameter.data.attributes["id"];
    if (emptyParameter(parameter) == true)
    {
        if (order == "Framework")
        {
            ajaxShow("Framework", "PATCH", "/api/v1/countries/1/frameworks/" + currentStatus.currentFramework.id, { "Content-Type": "application/json", Authorization: "Bearer " + token }, parameter, refreshLeftColumn);
            backToCreate("Framework");
        }
        else if (order == "Level")
        {
            ajaxShow("Level", "PATCH", "/api/v1/frameworks/" + currentStatus.currentFramework.id + "/levels/" + currentStatus.currentLevel.id, { "Content-Type": "application/json", Authorization: "Bearer " + token }, parameter, refreshLeftColumn);
            backToCreate("Level");
        }
        else if (order == "Section")
        {
            ajaxShow("Level", "PATCH", "/api/v1/levels/" + currentStatus.currentLevel.id + "/sections/" + currentStatus.currentSection.id, { "Content-Type": "application/json", Authorization: "Bearer " + token }, parameter, refreshLeftColumn);
            backToCreate("Section");
        }
        else if (order == "Objective")
        {
            ajaxShow("Objective", "PATCH", "/api/v1/sections/" + currentStatus.currentSection.id + "/objectives/" + currentStatus.currentObjective.id, { "Content-Type": "application/json", Authorization: "Bearer " + token }, parameter, refreshLeftColumn);
            backToCreate("Objective");
        }
    }
});



//check whether all the parameters are placed or not
function emptyParameter(parameter)
{
    $(".error").html("");
    let order = currentTop;
    var list = Object.keys(parameter.data.attributes);
    for (let i = 0; i < list.length; i++)
    {
        if (parameter.data.attributes[list[i]] == "" && inputFields[order][i].type != "checkbox" &&  inputFields[order][i].type != "textarea")
        {
            $("body").find("#" + order + list[i] + "error").html("The field is empty");
            return "Empty Field";
        }
    }
    return true;
}
//duplicating the sidebars
for (let i = 0; i < 4; i++)
{
    var paneContent = `<div class="sidebar-title">
                        ` + Heirarchy[i] + `
                      </div>
                      <div class="arrowContainer">
                        <div class="right-arrow"></div>
                      </div>
                      <div class="mainContent">
                        <div class="colContainer">
                          <div class="col1">
                            <div class="title">Saved ` + Heirarchy[i] + `</div>
                            <div class="listOfPresent" id="present` + Heirarchy[i] + `"">
                            </div>
                          </div>
                          <div class="col2">

                            <div class="title">New ` + Heirarchy[i] + `</div>
                            <div class="listOfPresent">
                            <div class="form">`;
    for (var j = 0; j < inputFields[Heirarchy[i]].length; j++)
    {
        if (inputFields[Heirarchy[i]][j].type == "checkbox")
        {
            paneContent += `<label class="form-switch">
        <input type="checkbox"  id="` + Heirarchy[i] + '-' + inputFields[Heirarchy[i]][j].name + `">
        <i></i> ` + inputFields[Heirarchy[i]][j].placeholder + ` </label>
                  <div class="error" id="` + Heirarchy[i] + inputFields[Heirarchy[i]][j].name + `error"></div>`;
        }
        else if (inputFields[Heirarchy[i]][j].type == "textarea")
        {
            paneContent += `  <div class="field">
    <textarea rows="5" cols="40" minlength="1" name="` + Heirarchy[i] + '-' + inputFields[Heirarchy[i]][j].name + `" id="` + Heirarchy[i] + '-' + inputFields[Heirarchy[i]][j].name + `" required placeholder=" "></textarea>
    <label class="label-checkbox" for="">` + inputFields[Heirarchy[i]][j].placeholder + `</label>
  </div><div class="error" id="` + Heirarchy[i] + inputFields[Heirarchy[i]][j].name + `error"></div>`
        }
        else
        {
            paneContent += `<div class="user-input-wrp">
                      <input placeholder=`+inputFields[Heirarchy[i]][j].placeholder+` type="` + inputFields[Heirarchy[i]][j].type + `" class="inputText" id="` + Heirarchy[i] + '-' + inputFields[Heirarchy[i]][j].name + `" required="" value="">
                
                    </div><div class="error" id="` + Heirarchy[i] + inputFields[Heirarchy[i]][j].name + `error"></div>`;
        }
    }
    paneContent += `<button class = "save" id="save` + Heirarchy[i] + `">Save</button>
                  <button class ="update invisible" id="update` + Heirarchy[i] + `">Update</button>
                  <button class = "discard" id="discard` + Heirarchy[i] + `">Discard</button>

`
    if (i != 3)
        paneContent += `<div class="sidebarLink add freeze ` + Heirarchy[i] + `" id="addNew` + Heirarchy[i + 1] + `"> Add ` + Heirarchy[i + 1] + ` to ` + Heirarchy[i] + `</div> `

    paneContent += ` </div> </div> </div> </div></div>`;
    $("#add" + Heirarchy[i]).html(paneContent);
}

// adding breadcrumbs statically
var sidebarList = document.getElementsByClassName('mainContent');
var html;
for (let i = 0; i < 4; i++)
{
    var template = `<div class="breadCrumb">`;
    for (var j = 0; j < i; j++) {
        var temp = "current" + Heirarchy[j];
        template += `<div class="singleBC" id="crumb` + Heirarchy[j] + `">
                  <div class="label notCurrentLabel">
                    ` + Heirarchy[j] + `
                  </div>
                  <div class="content notCurrent">
                    ` + currentStatus[temp].label + `
                  </div>
                </div>
                `
    }
    template += `<div class="singleBC current">
                Manage
                ` + Heirarchy[j] + `s
                </div>`;
    template += '</div>';
    html = $.parseHTML(template);
    sidebarList[i].insertBefore(html[0],sidebarList[i].children[0]) ;
}
//discarding the fields
$("body").on("click", ".discard", function() {
    discard(currentTop);
});

function discard(order)
{
    backToCreate(order);
    if ($("#add" + order).find(".discard").html() == "Discard")
    {
        $("#add" + order).find(".discard").html("Discard")
    }
    $("." + order).addClass('freeze');
    $("#add" + order).find("input:not([type='checkbox'])").val("");
    $("#add" + order+" textarea").val("");
    $(".error").html("");
}
//delete list item in each pane
$("body").on("click", ".delete", function()
{
  let order = currentTop;
  $(".siteContent").addClass('freeze')
  $("." + order).addClass('freeze');
  Delete_order = currentTop;
  Error.globalWarningConfirm(order)
  Delete_id = $(this).parent().parent().attr("id").trim();
});
$(".reject").on("click", function()
    {
        $(".siteContent").removeClass('freeze');
        $(".globalWarningConfirm").css("bottom", "-3.125rem");
    });
//edit list itrm on each pane
$("body").on("click", ".edit", function()
{
    let order = currentTop;
    $("#add" + order).find(".discard").html("Cancel");
    $("." + order).removeClass('freeze');
    $("#add" + order).find(".save").addClass('invisible');
    $("#add" + order).find(".update").removeClass('invisible');
    var data = JSON.parse($(this).parent().parent().attr("data"));
    if (order == "Level")
        delete data["achievable_xp"];
    var list = Object.keys(data); //Array of all the keys like title, description
    var listOfInputFields = $("#add" + order).find(" input , textarea");
    for (let i = 0; i < list.length; i++)
    {
        if (data[listOfInputFields[i].id.slice(order.length + 1)] == true || data[listOfInputFields[i].id.slice(order.length + 1)] == false)
        {
            listOfInputFields[i].checked = data[listOfInputFields[i].id.slice(order.length + 1)];
            currentStatus["current" + order][listOfInputFields[i].id.slice(order.length + 1)] = data[listOfInputFields[i].id.slice(order.length + 1)];
        }
        else
        {
            listOfInputFields[i].value = data[listOfInputFields[i].id.slice(order.length + 1)];
            currentStatus["current" + order][listOfInputFields[i].id.slice(order.length + 1)] = data[listOfInputFields[i].id.slice(order.length + 1)];
        }
    }
    currentStatus["current" + order]["id"] = $(this).parent().parent()[0].id;
});

//Loader display functions
$(document).ajaxStart(function()
{
    $(".loader").removeClass("invisible").addClass("display");
});

$(document).ajaxStop(function()
{
    $(".loader").removeClass("display").addClass("invisible");
});

// global error



function backToCreate(order)
{
    $("#add" + order).find(".discard").html("Discard");
    $("#add" + order).find(".update").addClass('invisible');
    $("#add" + order).find(".save").removeClass('invisible');
}

function refreshLeftColumn(order)
{
    if (order == "Framework")
    {
        ajaxShow("Framework", "GET", "/api/v1/countries/1/frameworks", { "Content-Type": "application/json", Authorization: "Bearer " + token }, "");
    }
    else if (order == "Level")
    {
        ajaxShow("Level", "GET", "/api/v1/frameworks/" + currentStatus.currentFramework.id + "/levels", { "Content-Type": "application/json", Authorization: "Bearer " + token }, "");
    }
    else if (order == "Section")
    {
        ajaxShow("Section", "GET", "/api/v1/levels/" + currentStatus.currentLevel.id + "/sections", { "Content-Type": "application/json", Authorization: "Bearer " + token }, "");
    }
    else if (order == "Objective")
    {
        ajaxShow("Objective", "GET", "/api/v1/sections/" + currentStatus.currentSection.id + "/objectives", { "Content-Type": "application/json", Authorization: "Bearer " + token }, "");
    }
}

function setBackground()
{
    let background=[`https://images.unsplash.com/photo-1500835556837-99ac94a94552`,
                    `https://images.unsplash.com/photo-1470922792794-b15b12d20e80`,
                    `https://images.unsplash.com/photo-1519213887655-a4f199e3015b`,
                    `https://images.unsplash.com/photo-1476778642660-a2c55571cf0d`,
                    `https://images.unsplash.com/photo-1465844880937-7c02addc633b`]
    let numOfSidebar=$(".sidebar").length;

    for(let i=0;i<numOfSidebar;i++)
    {
        let num=(Math.floor(Math.random()*100))%4;
        console.log("num",num);
        $(".sidebar")[i].style.backgroundImage="url("+background[num]+")";
    }
}
$("#level-up-option").on("click",function(){
    levelup();
});
$("#signOut-option").on("click",function(){
    localStorage.clear()
    window.location.href="./"
});
$("#admin-option").on("click",function(){
    
    window.location.href="./admin"
});
function browserWidth()
{
    if(window.navigator.userAgent.indexOf("Trident")!=-1 && $("body").width()>1074)
    {
        // $(".list").width(800);
        
        // console.log(width)
        // $(".list").width(width);
    }
    else
    {
        $(".sidebar").addClass('chrome-transition');
    }
}