from flask import Flask, render_template

app = Flask(__name__)

# Note to Aman:
# The commented-out code below is not required.
#
# .flaskenv sets FLASK_ENV=development
# for development environments,
#
# and Heroku sets FLASK_ENV=production
# for production environments
#
# The commented-out code below forces 'development' mode even on
# 'production' environments, and is, therefore, insecure.
#
app.debug=True
app.config['TEMPLATES_AUTO_RELOAD'] = True

@app.route("/")
def index():
    return render_template("index.html")

#Framework, level, section, objective CRUD

@app.route("/admin")
def function():
	return render_template("admin.html")
@app.route("/dashboard1")
def dashboard1():
	return render_template("dashboard1.html")

#User Onboarding
@app.route("/signup")
def signup():
    return render_template("sign_up.html")
@app.route("/olddashboard")
def Olddashboard():
    return render_template("old_dashboard.html")
@app.route("/dashboard")
def dashboard():
    return render_template("dashboard1.html")
@app.route("/switch")
def switch():
    return render_template("switch.html")
@app.route("/edit")
def editProfile():
    return render_template("editProfile.html")


