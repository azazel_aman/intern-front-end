import 'jquery'
import '../sass/dashboard1.sass'
// import '../sass/template.sass'
import '../font-awesome/fontawesome.css';
import * as Error from './error'
// import {IconName} from '@fortawesome/fontawesome'
import 'animate.css'
import 'normalize.css'
var base="https://kn-career-matrix.herokuapp.com"
var token;
var emp_id;

var current_objective;
var current_section;
var flag="0";
var active_state={
    "basic":{
        "email": undefined,
        "name":undefined,
        "kn_code": undefined,
        "admin":undefined,
        "achievable_xp": undefined,
        "accumulated_xp": undefined
    },
    "country":{
        "name": undefined,
        "active": undefined
    },
    "framework":{
        "label": undefined,
        "active": undefined,
        "accumulated_xp": undefined,
        "achievable_xp": undefined
    },
    "level":{
        "label": undefined,
        "threshold_xp":undefined,
        "achievable_xp":undefined,
        "active":undefined,
        "completed":undefined,
        "completed_at":undefined
    },
    "country_id":undefined,
    "framework_id":undefined,
    "level_id":undefined
};
var clicked={
	"clicked_level":{
		"label": undefined,
		"threshold_xp":undefined,
		"achievable_xp":undefined,
		"active":undefined,
		"completed":undefined,
		"completed_at":undefined
	},
	"clicked_section":{
		"label" : undefined
	},
	"clicked_objective":{
		"completed": undefined,
		"completed_at": undefined,
		"description": undefined,
		"essential": undefined,
		"title": undefined,
		"xp":undefined
	}
	,
	"clicked_level_id": undefined,
	"clicked_section_id": undefined,
	"clicked_objective_id": undefined,
	"level_div":undefined,
	"objective_div":undefined,
	"section_div":undefined
};
$(document).ajaxStart(function() {
  $(".loader").removeClass("invisible").addClass("display-loader");
}).ajaxStop(function() {
  $(".loader").removeClass("display-loader").addClass("invisible");
});
$(document).ready(function(){
	if(localStorage.getItem("flag")=="1")
	{
		$(".splash").remove();
	}
	// setBackground();

    token=localStorage.getItem("token");
    emp_id=localStorage.getItem("emp_id");
    getDashboard(populateDashboard);
    browserWidth();
    
    
});
$("body").on("click","#removeSplash",function(){
    $(".splash").remove();
    $(".siteContent").removeClass("invisible").addClass("display");
})
function setClicked(data)
{
	if(data.type=="employee_level")
	{
		let listOfKeys=Object.keys(clicked.clicked_level);
		for(let i =0;i<listOfKeys.length;i++)
		{
			clicked.clicked_level[listOfKeys[i]]=data.attributes[listOfKeys[i]]
		}
		clicked.clicked_level_id=data.id;
	}
	else if(data.type=="employee_objective")
	{
		let listOfKeys=Object.keys(clicked.clicked_objective);
		for(let i =0;i<listOfKeys.length;i++)
		{
			clicked.clicked_objective[listOfKeys[i]]=data.attributes[listOfKeys[i]]
		}
		clicked.clicked_objective_id=data.id;
	}
	else if(data.type=="employee_section")
	{
		let listOfKeys=Object.keys(clicked.clicked_section);
		for(let i =0;i<listOfKeys.length;i++)
		{
			clicked.clicked_section[listOfKeys[i]]=data.attributes[listOfKeys[i]]
		}
		clicked.clicked_section_id=data.id;
	}
}
function getDashboard(callback)
{
    let settings = {
      "async": true,
      "crossDomain": true,
      "url": base+"/api/v1/dashboard",
      "method": "GET",
      "headers": {
        "Content-Type": "application/json",
        "Authorization": "Bearer "+token,
      },
      "success": function(data){
        setActiveState(data);
        callback(data);
        if(data.data.attributes.admin==false)
        {
        	$("#admin-option").remove();
        }
        
      },
      "error": function(error){
        window.location.href = "https://kn-career-matrix-front-end.herokuapp.com/?message=failure";
      }
    }
    $.ajax(settings)
}
function populateDashboard(data)
{
	setActiveState(data);
	console.log(data);
	$("#showLevel .sidebar-title").html(active_state.framework.label);
	populateBasics();
	data.meta.active_employee_framework.relationships.employee_levels.data=data.meta.active_employee_framework.relationships.employee_levels.data.sort(function(a,b){
		return -a.attributes.threshold_xp + b.attributes.threshold_xp;
	})

    let listOfLevels=data.meta.active_employee_framework.relationships.employee_levels.data;
    let html="";
    let secondlip=`<div class="title-bar">
                    <div class="leftbox">
                    <div class="title">
<div class="text-title baseline">
	                        <div class="framework-title">`+active_state.framework.label+`</div>&nbsp;<div class="country-title">`+active_state.country.name+`</div>
                    </div>
</div>
                    </div>
                    <div class="rightbox">
                    
                    <div class="badge-wrapper">
                    
                    </div>
                    <div class="xp xp-title">
                        <div class="label">XP&nbsp;</div>
                        <div class="value">`+data.meta.active_employee_framework.attributes.accumulated_xp+`<span class="slash">/</span>`+data.meta.active_employee_framework.attributes.achievable_xp+`</div>
                    </div>
                    </div>
                </div>`
    $("#showLevel .main").prepend(secondlip);
    let flag=false;
    for(let i=0;i<listOfLevels.length;i++)
    {
    	if(flag==false)
    	{
    		if(listOfLevels[i].attributes.active==true)
    		{
    		    		  	html+=`<div class="list pointer" data-id='`+listOfLevels[i].id+`' data-attributes='`+JSON.stringify(listOfLevels[i])+`'>  
    		                    <div class="basic-info">
    		                        <div class="leftbox">
    		                        <div class="title list-font">
    		                            `+listOfLevels[i].attributes.label+`
    		                        </div>

    		                        </div>
    		                        <div class="rightbox">
    		                        
    		                        <div class="badge-wrapper">
    		                        <div class="badge active" data-status="active">
    		                            Active
    		                        </div>
    		                        </div>
    		                        <div class="xp">
    		                            <div class="label">XP&nbsp;</div>
    		                            <div class="value">`+data.meta.active_employee_framework.relationships.employee_levels.data[i].attributes.accumulated_xp+`<span class="slash">/</span>`+data.meta.active_employee_framework.relationships.employee_levels.data[i].attributes.achievable_xp+`</div>
    		                        </div>
    		                        </div>
    		                    </div>
    		                    <div class="stat-container"> 
    		                        <div class="completion-date">&nbsp;</div>
    		                        <div class="stats baseline">  
    		                                <div class="label"> Sections</div>
    		                                <div class="value">`+data.meta.active_employee_framework.relationships.employee_levels.data[i].relationships.employee_sections.data.length+`</div>
    		                                <div class="divider">|</div>
    		                                <div class="label">  Objectives</div>
    		                                <div class="value"> `;
    		                  var numberOfObjectives=0;
    		                  for(let j=0;j<data.meta.active_employee_framework.relationships.employee_levels.data[i].relationships.employee_sections.data.length;j++)
    		                  {
    		                  	numberOfObjectives+=data.meta.active_employee_framework.relationships.employee_levels.data[i].relationships.employee_sections.data[j].relationships.employee_objectives.data.length;
    		                  }
    		                  html+=numberOfObjectives+`</div>
    		                        </div>
    		                    </div>
    		                </div>
    		`;
    		flag=true;
    		}
    		else
    		{
    					  	html+=`<div class="list pointer" data-id='`+listOfLevels[i].id+`' data-attributes='`+JSON.stringify(listOfLevels[i])+`'>  
    			                    <div class="basic-info">
    			                        <div class="leftbox">
    			                        <div class="title list-font">
    			                            `+listOfLevels[i].attributes.label+`
    			                        </div>

    			                        </div>
    			                        <div class="rightbox">
    			                        
    			                        <div class="badge-wrapper">
    			                        <div class="badge locked" data-status="locked">
    			                            Locked
    			                        </div>
    			                        </div>
    			                        <div class="xp">
    			                            <div class="label">Threshold XP&nbsp;</div>
    			                            <div class="value">`+data.meta.active_employee_framework.relationships.employee_levels.data[i].attributes.threshold_xp+`</div>
    			                        </div>
    			                        </div>
    			                    </div>
    			                    <div class="stat-container"> 
    			                        <div class="completion-date">&nbsp;</div>
    			                        <div class="stats baseline">  
    			                                <div class="label"> Sections</div>
    			                                <div class="value">`+data.meta.active_employee_framework.relationships.employee_levels.data[i].relationships.employee_sections.data.length+`</div>
    			                                <div class="divider">|</div>
    			                                <div class="label">  Objectives</div>
    			                                <div class="value"> `;
    			                  var numberOfObjectives=0;
    			                  for(let j=0;j<data.meta.active_employee_framework.relationships.employee_levels.data[i].relationships.employee_sections.data.length;j++)
    			                  {
    			                  	numberOfObjectives+=data.meta.active_employee_framework.relationships.employee_levels.data[i].relationships.employee_sections.data[j].relationships.employee_objectives.data.length;
    			                  }
    			                  html+=numberOfObjectives+`</div>
    			                        </div>
    			                    </div>
    			                </div>
    			`;
    		}
    			
    	}
    	
    	
    	else
    	{
    		if( listOfLevels[i].attributes.completed == true)
    		{
    			    		  	html+=`<div class="list pointer" data-id='`+listOfLevels[i].id+`' data-attributes='`+JSON.stringify(listOfLevels[i])+`'>  
    			                    <div class="basic-info">
    			                        <div class="leftbox">
    			                        <div class="title list-font">
    			                            `+listOfLevels[i].attributes.label+`
    			                        </div>

    			                        </div>
    			                        <div class="rightbox">
    			                        
    			                        <div class="badge-wrapper">
    			                        <div class="badge completed"  data-status="completed">
    			                            Completed
    			                        </div>
    			                        </div>
    			                        <div class="xp">
    			                            <div class="label">XP&nbsp;</div>
    			                            <div class="value">`+data.meta.active_employee_framework.relationships.employee_levels.data[i].attributes.accumulated_xp+`<span class="slash">/</span>`+data.meta.active_employee_framework.relationships.employee_levels.data[i].attributes.achievable_xp+`</div>
    			                        </div>
    			                        </div>
    			                    </div>
    			                    <div class="stat-container"> 
    			                        <div class="completion-date">Completed on&nbsp;`+data.meta.active_employee_framework.relationships.employee_levels.data[i].attributes.completed_at.split(" ").splice(0,4).join(" ")+`</div>
    			                        <div class="stats baseline">  
    			                                <div class="label"> Sections</div>
    			                                <div class="value">`+data.meta.active_employee_framework.relationships.employee_levels.data[i].relationships.employee_sections.data.length+`</div>
    			                                <div class="divider">|</div>
    			                                <div class="label">  Objectives</div>
    			                                <div class="value"> `;
    			                         
    			                  var numberOfObjectives=0;
    			                  for(let j=0;j<data.meta.active_employee_framework.relationships.employee_levels.data[i].relationships.employee_sections.data.length;j++)
    			                  {
    			                  	numberOfObjectives+=data.meta.active_employee_framework.relationships.employee_levels.data[i].relationships.employee_sections.data[j].relationships.employee_objectives.data.length;
    			                  }
    			                  html+=numberOfObjectives+`</div>
    			                        </div>
    			                    </div>
    			                </div>
    			`;
    		}
    		else
    		{
    			  		  	html+=`<div class="list pointer" data-id='`+listOfLevels[i].id+`' data-attributes='`+JSON.stringify(listOfLevels[i])+`'>  
    		                    <div class="basic-info">
    		                        <div class="leftbox">
    		                        <div class="title list-font">
    		                            `+listOfLevels[i].attributes.label+`
    		                        </div>

    		                        </div>
    		                        <div class="rightbox">
    		                        
    		                        <div class="badge-wrapper">
    		                        <div class="badge locked"  data-status="skipped">
    		                            Skipped
    		                        </div>
    		                        </div>
    		                         <div class="xp">
                                        <div class="label">XP&nbsp;</div>
                                        <div class="value">`+data.meta.active_employee_framework.relationships.employee_levels.data[i].attributes.accumulated_xp+`<span class="slash">/</span>`+data.meta.active_employee_framework.relationships.employee_levels.data[i].attributes.achievable_xp+`</div>
                                    </div>
    		                        </div>
    		                    </div>
    		                    <div class="stat-container"> 
    		                        <div class="completion-date">&nbsp;</div>
    		                              <div class="stats baseline">  
                                                  <div class="label"> Sections</div>
                                                  <div class="value">`+data.meta.active_employee_framework.relationships.employee_levels.data[i].relationships.employee_sections.data.length+`</div>
                                                  <div class="divider">|</div>
                                                  <div class="label">  Objectives</div>
                                                  <div class="value"> `;
                                    var numberOfObjectives=0;
                                    for(let j=0;j<data.meta.active_employee_framework.relationships.employee_levels.data[i].relationships.employee_sections.data.length;j++)
                                    {
                                        numberOfObjectives+=data.meta.active_employee_framework.relationships.employee_levels.data[i].relationships.employee_sections.data[j].relationships.employee_objectives.data.length;
                                    }
                                    html+=numberOfObjectives+`</div>
                                          </div>
                                      </div>
    		                    </div>
    		                </div>
    		`;	
    		}	
    	}
        browserWidth();
    }
    $("#showLevel .listContainer").html(html);
    browserWidth();
    $(".loader").removeClass("display-loader").addClass("invisible");
    if(localStorage.getItem("flag")=="1")
    {
    		$(".splash").remove();
    		$(".siteContent").removeClass("invisible").addClass("display");
    }
    else
    {
    	$("#removeSplash").removeClass("invisible").addClass("display");
    }
    localStorage.setItem("flag", "1");
    
    
    checkForLevelUp();
}
function setActiveState(data)
{
    //set basics
    let listOfKeys=Object.keys(data.data.attributes);
    for(let i=0;i<listOfKeys.length;i++)
    {
        let tag=listOfKeys[i];
        active_state.basic[tag]=data.data.attributes[tag];
    }
    //set active country
    listOfKeys=Object.keys(data.meta.active_employee_country.attributes);
    for(let i=0;i<listOfKeys.length;i++)
    {
        let tag=listOfKeys[i];
        active_state.country[tag]=data.meta.active_employee_country.attributes[tag];
    }
    active_state.country_id=data.meta.active_employee_country.id;
    //set active framework
    listOfKeys=Object.keys(data.meta.active_employee_framework.attributes);
    for(let i=0;i<listOfKeys.length;i++)
    {
        let tag=listOfKeys[i];
        active_state.framework[tag]=data.meta.active_employee_framework.attributes[tag];
    }
    active_state.framework_id=data.meta.active_employee_framework.id;
    //set active Level
    let listOfLevels=data.meta.active_employee_framework.relationships.employee_levels.data;
    for(let i=0;i<listOfLevels.length;i++)
    {
            if(listOfLevels[i].attributes.active==true)
            {
                listOfKeys=Object.keys(listOfLevels[i].attributes)
                for(let j=0;j<listOfKeys.length;j++)
                {
                    let tag=listOfKeys[j];
                    active_state.level[tag]=listOfLevels[i].attributes[tag]
                }
                active_state.level_id=listOfLevels[i].id;
                break;
            }
    }
}
function populateSection(status,data,callback)
{

	$("#showSection .main .title-bar").remove();
	
	let html=``;
       html+=`<div class="title-bar">
                    <div class="leftbox">
                    <div class="title">
                        `+data.attributes.label+`
                    </div>

                    </div>
                    <div class="rightbox">
                    
                    <div class="badge-wrapper">
                    <div class="badge `+status+`">
                        `+status+`
                    </div>
                    </div>
                    <div class="xp xp-title">`
                    if(status=="locked")
                    {
                    	html+=`<div class="label">Threshold XP&nbsp;</div>
                        <div class="value">`+data.attributes.threshold_xp+`</div>`
                    }
                    else
                    {
                    		html+=`<div class="label">XP&nbsp;</div>
                    	    <div class="value">`+data.attributes.accumulated_xp+`<span class="slash">/</span>`+data.attributes.achievable_xp+`</div>`	
                    }
                    
                    html+=`
                        
                    </div>
                    </div>
                </div>`
                $("#showSection .main").prepend(html);
				html=``;
                for(let i=0;i<data.relationships.employee_sections.data.length;i++)
                {
                    var better=JSON.parse(JSON.stringify(data.relationships.employee_sections.data[i]));
                    delete better.relationships;
                	html+=`<div class="list"  data-attributes='`+JSON.stringify(better)+`'>
                        <div class="section-container">
                            <div class="section-title">`+data.relationships.employee_sections.data[i].attributes.label+`</div>
                        </div>`
                        for(let j=0;j<data.relationships.employee_sections.data[i].relationships.employee_objectives.data.length;j++)
                        {

                        html+=`
                        <div class="each-objective">
                            <div class="section-objective">
                                <div class="objective"  data-attributes='`+JSON.stringify(data.relationships.employee_sections.data[i].relationships.employee_objectives.data[j])+`'>
                                    `
                                    
                                    html+=`
                                    <div class="leftbox">`
                                    if(data.relationships.employee_sections.data[i].relationships.employee_objectives.data[j].attributes.completed==true)
                                    {
                                    	html+=`<i class="denoter fas fa-check-circle objective-completed"></i>`;
                                    }
                                    else
                                    {
                                    	html+=`<i class="denoter far fa-check-circle"></i>`;	
                                    }

                                    html+=`              
                                            <div class="objective-title">`+data.relationships.employee_sections.data[i].relationships.employee_objectives.data[j].attributes.title+`</div>
                                    </div>
                                    <div class="rightbox">
                                    `
                                    if(data.relationships.employee_sections.data[i].relationships.employee_objectives.data[j].attributes.essential==true)
                                    {
                                    	html+=`
                                    	<div class="badge-wrapper">
                                    	    <div class="badge">Essential</div> 
                                    	</div>`
                                    }
                                    else
                                    {
                                    	html+=`
                                    	<div class="badge-wrapper">
                                    	    <div class="badge skipped">Essential</div> 
                                    	</div>`
                                    }
                                    html+=`
                                        <div class="xp">
                                            <div class="label">XP&nbsp;</div>
                                            <div class="value">`+data.relationships.employee_sections.data[i].relationships.employee_objectives.data[j].attributes.xp+`</div>
                                        </div>
                                    </div>
                                 </div> 
                            </div></div>`
					}
                            html+=`
                        </div>
                    </div>`;
                    }
html+=`</div>`                             	
                
                       html+= `

                            
                        </div>

      
                    </div>
                  `;
       
	// for()
	$("#showSection .listContainer").html(html);
    browserWidth();
    callback();
}
function populateBasics()
{
	localStorage.setItem("name",active_state.basic.name)
	localStorage.setItem("kn_code",active_state.basic.kn_code);
	localStorage.setItem("total_xp",active_state.basic.accumulated_xp);
    $("#name").html(active_state.basic.name);
    $("#kn_code").html(active_state.basic.kn_code);
    $("#accumulated_xp").html(active_state.basic.accumulated_xp);
    $("#achievable_xp").html(active_state.basic.achievable_xp);
}

$("body").on("click","#showSection .denoter",function(){
	clicked.objective_div=$(this).parent().parent();
	clicked.section_div=$(this).parent().parent().parent().parent().parent();
	var divElement=$(this)
    $(".denoter").addClass("disable-pointer-events");
	let temp=((divElement.parent().parent().attr("data-attributes")));
	setClicked(JSON.parse(temp));
	let settings = {
	      "async": true,
	      "crossDomain": true,
	      "url": base+"/	api/v1/employee_objectives/"+clicked.clicked_objective_id+"/toggle_complete",
	      "method": "PATCH",
	      "headers": {
	        "Content-Type": "application/json",
	        "Authorization": "Bearer "+token,
	      },
	      "success": function(data){
	        let change=JSON.parse(clicked.objective_div.attr("data-attributes"))
            let temp=JSON.parse(clicked.level_div.attr("data-attributes")).attributes.accumulated_xp;
	        change.attributes=data.data.attributes;
	        divElement.parent().parent().attr("data-attributes",JSON.stringify(change));
	        let section_id=JSON.parse(clicked.section_div.attr("data-attributes")).id;
	        
	        	for(let i=0;i<JSON.parse(clicked.level_div.attr("data-attributes")).relationships.employee_sections.data.length;i++)
	        {
	        	if(JSON.parse(clicked.level_div.attr("data-attributes")).relationships.employee_sections.data[i].id==section_id)
	        	{
	        		for(let j=0;j<JSON.parse(clicked.level_div.attr("data-attributes")).relationships.employee_sections.data[i].relationships.employee_objectives.data.length;j++)
	        		{
	        			if(JSON.parse(clicked["level_div"].attr("data-attributes")).relationships.employee_sections.data[i].relationships.employee_objectives.data[j].id==JSON.parse(clicked["objective_div"].attr("data-attributes")).id)
	        			{
	        				let a=JSON.parse(clicked.level_div.attr("data-attributes"));
	        					a.relationships.employee_sections.data[i].relationships.employee_objectives.data[j].attributes=data.data.attributes;
	        				if(JSON.parse(clicked.level_div.attr("data-attributes")).attributes.completed_at!=null || JSON.parse(clicked.level_div.attr("data-attributes")).attributes.active==true)
	        				{
                                if(data.data.attributes.completed==true)
                                {
                                    a.attributes.accumulated_xp+=data.data.attributes.xp;
                                }
                                else
                                {
                                 a.attributes.accumulated_xp-=data.data.attributes.xp;   
                                }
                                clicked.level_div.attr('data-attributes', JSON.stringify(a));
	        				}
	        				else
	        				{
	        						clicked.level_div.attr('data-attributes', JSON.stringify(a));	
	        				}
	        			}
	        		}
	        	}
	        }	
	        
	        
	        if(data.data.attributes.completed==true)
	        {
	        	divElement.removeClass('far').addClass('objective-completed').addClass('fas');
                
                $(".denoter").removeClass("disable-pointer-events");
                if(JSON.parse(clicked.level_div.attr("data-attributes")).attributes.completed_at!=null|| JSON.parse(clicked.level_div.attr("data-attributes")).attributes.active==true)
                {
                		        	let selectedLevelData=JSON.parse(clicked.level_div.attr("data-attributes"));
                	                let newXP=parseInt(parseInt(clicked.level_div.find('.xp .value').html().split(`<span div="slash">/</span>`)[0])+change.attributes.xp)+`<span class="slash">/</span>`+selectedLevelData.attributes.achievable_xp;        
                	                clicked.level_div.find(".basic-info .value").html(newXP);
                	                $("#showSection .title-bar").find(".value").html(newXP);
                	                newXP=parseInt(parseInt($("#showLevel .title-bar").find('.xp .value').html().split(`<span div="slash">/</span>`)[0])+change.attributes.xp)+`<span class="slash">/</span>`+active_state.framework.achievable_xp;
                	                $("#showLevel .title-bar").find('.value').html(newXP);
                	                active_state.basic.accumulated_xp=active_state.basic.accumulated_xp+change.attributes.xp;
                	                
                	                $("#accumulated_xp").html(active_state.basic.accumulated_xp);
                }
                checkForLevelUp();
                
	        }
	        else
                {
                divElement.addClass('far').removeClass('objective-completed').removeClass('fas');
				$(".denoter").removeClass("disable-pointer-events");                
                if(JSON.parse(clicked.level_div.attr("data-attributes")).attributes.completed_at!=null|| JSON.parse(clicked.level_div.attr("data-attributes")).attributes.active==true)
                {
                	let selectedLevelData=JSON.parse(clicked.level_div.attr("data-attributes"));
                	let newXP=parseInt(parseInt(clicked.level_div.find('.xp .value').html().split(`<span div="slash">/</span>`)[0])-change.attributes.xp)+`<span class="slash">/</span>`+selectedLevelData.attributes.achievable_xp;
                	clicked.level_div.find(".basic-info .value").html(newXP);
                	$("#showSection .title-bar").find(".value").html(newXP);
                	newXP=parseInt(parseInt($("#showLevel .title-bar").find('.xp .value').html().split(`<span div="slash">/</span>`)[0])-change.attributes.xp)+`<span class="slash">/</span>`+active_state.framework.achievable_xp;
                	$("#showLevel .title-bar").find('.value').html(newXP);
                	active_state.basic.accumulated_xp=active_state.basic.accumulated_xp-change.attributes.xp
                	$("#accumulated_xp").html(active_state.basic.accumulated_xp)	
                }
                checkForLevelUp();

            }},
	      "error": function(error){
	        console.log(error);
	        checkForLevelUp();
	        $(".denoter").removeClass("disable-pointer-events");
	        Error.globalWarning(error.responseJSON.errors[0].detail)
	      }
	    }
	    $.ajax(settings)
})

$("#showLevel .sidebar-title").on("click",function()
{
if(window.navigator.userAgent.indexOf("Trident")!=-1)
{
    console.log("aman");
    if($(window).width()<= 1074) 
        {
            $("#showSection").css({
                "-webkit-transform": "translateY(100vh)",
                "-ms-transform": "translateY(100vh)",
                "transform": "translateY(100vh)"
            }); 
        } 
    else 
        {
            $("#showSection").css({
                "-webkit-transform": "translateX(100vw)",
                "-ms-transform": "translateX(100vw)",
                "transform": "translateX(100vw)"
            }); 
        }
}
else
{
    if($(window).width()<= 1074) 
        {
            $("#showSection").css({
                "bottom": "-100vh"
            }); 
        } 
    else 
        {
            $("#showSection").css({
                "right": "-100vw"
            }); 
        }  
}
    
	
});

$("body").on("click","#showLevel .list",function()
{
    clicked.level_div=$(this);
    $("#showSection .sidebar-title").html($(this).find(".title").html().trim());
    populateSection($(this).find(".badge").attr("data-status"),JSON.parse($(this).attr("data-attributes")),()=>{
if(window.navigator.userAgent.indexOf("Trident")!=-1)
{
    console.log("hel[p")
    if($(window).width()<= 1074) 
        {
            $("#showSection").css({
                "-webkit-transform": "translateY(-100vh)",
                "-ms-transform": "translateY(-100vh)",
                "transform": "translateY(-100vh)"
            }); 
        } 
    else 
        {
            $("#showSection").css({
                "-webkit-transform": "translateX(-100vw)",
                "-ms-transform": "translateX(-100vw)",
                "transform": "translateX(-100vw)"
            }); 
        }
}
else
{
    if($(window).width()<= 1074) 
        {
            $("#showSection").css({
                "bottom": "0"
            }); 
        } 
    else 
        {
            $("#showSection").css({
                "right": "0"
            }); 
        }  
}

    });
    setClicked(JSON.parse($(this).attr("data-attributes")));
    
});
$("*").dblclick(function(e)
{
    e.preventDefault();
});



$(".edit-option").on("click",function(){
	window.location.href="./"
})
function levelup(){
	let settings = {
	  "async": true,
	  "crossDomain": true,
	  "url": base+"/api/v1/transition",
	  "method": "POST",
	  "headers": {
	    "Content-Type": "application/json",
	    "Authorization": "Bearer "+token,
	  },
      "data":JSON.stringify({"meta": {"transition_type": "level_up"} }),
      "success": function(data){
	    location.reload();
	  },   
	  "error": function(error){
	    console.log(error);
	    Error.globalError(error.responseJSON.errors[0].detail)
	  }
	}
	$.ajax(settings)
}


//level up works

function checkForLevelUp()
{
	console.log(active_state)
	let flag=1
	let currentLevel=$(`#showLevel .list [data-status="active"]`).parent().parent().parent().parent();
	if(JSON.parse($("#showLevel .list").first().attr('data-attributes')).attributes.active!=true)
	{
		let nextLevel=$(`#showLevel .list [data-status="active"]`).parent().parent().parent().parent().prev();
			let currentdata=JSON.parse(currentLevel.attr("data-attributes"));
			for(let i=0;i<currentdata.relationships.employee_sections.data.length;i++)
			{
				for(let j=0;j<currentdata.relationships.employee_sections.data[i].relationships.employee_objectives.data.length;j++)
				{
						if(currentdata.relationships.employee_sections.data[i].relationships.employee_objectives.data[j].attributes.essential==true &&currentdata.relationships.employee_sections.data[i].relationships.employee_objectives.data[j].attributes.completed==false)
						{
							flag=0
						}	
				}
			}
			if(JSON.parse(nextLevel.attr("data-attributes")).attributes.threshold_xp>active_state.basic.accumulated_xp)
			{
				flag=0;
									// console.log("Xp not met")
									// console.log("Threshold",JSON.parse(nextLevel.attr("data-attributes")).attributes.threshold_xp)
									// console.log("Acc",active_state.basic.accumulated_xp)
				
			}
			

		if(flag==0)
		{
			$("#level-up-option").addClass('block')
		}
		else
		{
			$("#level-up-option").removeClass('block')	
		}
	}
	else
	{
			return 0;
				
	}

}

function setBackground()
{
    let background=[`https://images.unsplash.com/photo-1500835556837-99ac94a94552`,
                    `https://images.unsplash.com/photo-1470922792794-b15b12d20e80`,
                    `https://images.unsplash.com/photo-1519213887655-a4f199e3015b`,
                    `https://images.unsplash.com/photo-1476778642660-a2c55571cf0d`,
                    `https://images.unsplash.com/photo-1465844880937-7c02addc633b`]
    let numOfSidebar=$(".sidebar").length;

    for(let i=0;i<numOfSidebar;i++)
    {
        let num=(Math.floor(Math.random()*100))%4;
        console.log("num",num);
        $(".sidebar")[i].style.backgroundImage="url("+background[num]+")";
    }
}
$("#level-up-option").on("click",function(){
    levelup();
});
$("#signOut-option").on("click",function(){
    localStorage.clear()
    window.location.href="./"
});
$("#admin-option").on("click",function(){
    
    window.location.href="./admin"
});
function browserWidth()
{
    if(window.navigator.userAgent.indexOf("Trident")!=-1 && $("body").width()>1074)
    {
        $(".list").width(800);
        
        // console.log(width)
        // $(".list").width(width);
    }
    else
    {
        $(".sidebar").addClass('chrome-transition');
    }
}