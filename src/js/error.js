function globalSuccess(message)
{
    $(".globalSuccess").html(message);
    $(".globalSuccess").show(function(){
        $(".globalSuccess").removeClass("invisible").addClass("display");
      $(".globalSuccess").animate({"bottom":"0"},500).delay(5000).animate({"bottom":"-50px"},500,function(){
        $(".globalSuccess").removeClass("invisible").removeClass("display");
            $(".globalSuccess").hide();
      })  
    });
}
function globalError(message)
{
    $(".globalError").html(message);
    $(".globalError").show(function(){
        $(".globalError").removeClass("invisible").addClass("display");
      $(".globalError").animate({"bottom":"0"},500).delay(5000).animate({"bottom":"-50px"},500,function(){
        $(".globalError").removeClass("invisible").removeClass("display");
            $(".globalError").hide();
      })  
    });    
}
function globalWarning(message)
{
    $(".globalWarning").html(message);
    $(".globalWarning").show(function(){
        $(".globalWarning").removeClass("invisible").addClass("display");
      $(".globalWarning").animate({"bottom":"0"},500).delay(5000).animate({"bottom":"-50px"},500,function(){
        $(".globalWarning").removeClass("invisible").removeClass("display");
            $(".globalWarning").hide();
      })  
    });    
}
function globalStatus(message)
{
    $(".globalStatus").html(message);
    $(".globalStatus").show(function(){
        $(".globalStatus").removeClass("invisible").addClass("display");
      $(".globalStatus").animate({"bottom":"0"},500).delay(5000).animate({"bottom":"-50px"},500,function(){
        $(".globalStatus").removeClass("invisible").removeClass("display");
            $(".globalStatus").hide();
      })  
    });
}
function globalWarningConfirm(parameterOrder)
{
    $(".globalWarningConfirm div").first().html(`Do you want to delete this ` + parameterOrder + `?`);
    $(".globalWarningConfirm").show(function(){
        $(".globalWarningConfirm").removeClass("invisible").addClass("display");
      $(".globalWarningConfirm").animate({"bottom":"0"},500)  
    });
}
export {globalSuccess, globalError, globalWarning , globalStatus , globalWarningConfirm};