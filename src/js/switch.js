import '../sass/switch.sass'
// import '../sass/template.sass'
import 'jquery'
import '../font-awesome/fontawesome.css'
import * as Error from'./error'
// import '../sass/navbar_lip.sass'
// import '../sass/loader.sass'
// import '../sass/error.sass'
// import '../sass/media.sass'
// import '../sass/input.sass'
// import '../sass/sidebar.sass'
// import '../sass/scrollbar.sass'

var token = localStorage.getItem("token");
var Heirarchy = ['Basic', 'Option'];
var host = "https://kn-career-matrix.herokuapp.com";
var currentTop = 'UserDetails';
var emp_id;
var currentStatus =
{
    currentCountry:
    {
        type: "country",
        id: undefined
    },
    currentFramework:
    {
        type: "framework",
        id: undefined
    },
    currentLevel:
    {
        type: "level",
        id: undefined
    }
};
var dataTypes = {
    "text": "string",
    "number": "integer",
    "checkbox": "boolean",
    "textarea": "string"
}

$("*").dblclick(function(e)
{
    e.preventDefault();
});

//search algo for the search bar for country, framework and level form
// $("#searchCountry , #searchFramework , #searchLevel").on("keyup ", function()
// {
//     let val = $(this).val();
//     let listOfAllList = $(this).parent().parent().find(".list");
//     if (val == "")
//     {
//         for (let i = 0; i < listOfAllList.length; i++)
//         {
//             listOfAllList[i].style.classList.remove("invisible");
//             listOfAllList[i].style.className+= " display";
//         }
//     }
//     else
//     {
//         for (let i = 0; i < listOfAllList.length; i++)
//         {
//             if (listOfAllList[i].children[0].innerHTML.trim().toLowerCase().indexOf(val.trim().toLowerCase()) > -1)
//             {
//                 listOfAllList[i].style.classList.remove("invisible");
//                 listOfAllList[i].style.className+= " display";
//             }
//             else
//             {
//                 listOfAllList[i].style.classList.remove("display");
//                 listOfAllList[i].style.className+= " invisible";
//             }
//         }
//     }
// });
$(document).ready(function()
{
    ajaxGet("Country",  "/api/v1/countries",{"Content-Type": "application/json", "Authorization" : "Bearer " + token }) 
    $("#name").html(localStorage.getItem("name"));
    $("#kn_code").html(localStorage.getItem("kn_code"));
    $("#accumulated_xp").html(localStorage.getItem("total_xp"));
    browserWidth();
    
});
$("body").find("#addCountry #selectCountry").on("click", ".list", function()
{
    //chrome and firefox
    if(window.navigator.userAgent.indexOf("Trident")!=-1)
{
    if($(window).width()<= 1074) 
        {
            $("#addFramework").css({
                "-webkit-transform": "translateY(-100%)",
                "-ms-transform": "translateY(-100%)",
                "transform": "translateY(-100%)"
            }); 
        } 
    else 
        {
            $("#addFramework").css({
                "-webkit-transform": "translateX(-100vw)",
                "-ms-transform": "translateX(-100vw)",
                "transform": "translateX(-100vw)"
            }); 
        }
}
//ie11
else
{
    if($(window).width()<= 1074) 
        {
            $("#addFramework").css({
                "bottom": "0"
            }); 
        } 
    else 
        {
            $("#addFramework").css({
                "right": "0"
            }); 
        }  
}
    
    // $("#addFramework").css("right","0");
    // $("#selectFramework").removeClass("invisible").addClass("display");
    // $(".sidebar").css("background-color", "white");
});

$("body").find("#addFramework #selectFramework").on("click", ".list", function()
{
        if(window.navigator.userAgent.indexOf("Trident")!=-1)
    {
        if($(window).width()<= 1074) 
            {
                $("#addLevel").css({
                    "-webkit-transform": "translateY(-100%)",
                    "-ms-transform": "translateY(-100%)",
                    "transform": "translateY(-100%)"
                }); 
            } 
        else 
            {
                $("#addLevel").css({
                    "-webkit-transform": "translateX(-100vw)",
                    "-ms-transform": "translateX(-100vw)",
                    "transform": "translateX(-100vw)"
                }); 
            }
    }
    else
    {
        if($(window).width()<= 1074) 
            {
                $("#addLevel").css({
                    "bottom": "0"
                }); 
            } 
        else 
            {
                $("#addLevel").css({
                    "right": "0"
                }); 
            }  
    }
    // $("#addLevel").css("right","0");
    // $("#selectLevel").removeClass("invisible").addClass("display");
})
$("#addFramework .sidebar-title").on("click",function(){
        if(window.navigator.userAgent.indexOf("Trident")!=-1)
    {
        if($(window).width()<= 1074) 
            {
                $("#addLevel").css({
                    "-webkit-transform": "translateY(100%)",
                    "-ms-transform": "translateY(100%)",
                    "transform": "translateY(100%)"
                }); 
            } 
        else 
            {
                $("#addLevel").css({
                    "-webkit-transform": "translateX(100vw)",
                    "-ms-transform": "translateX(100vw)",
                    "transform": "translateX(100vw)"
                }); 
            }
    }
    else
    {
        if($(window).width()<= 1074) 
            {
                $("#addLevel").css({
                    "bottom": "-100%"
                }); 
            } 
        else 
            {
                $("#addLevel").css({
                    "right": "-100vw"
                }); 
            }  
    }
    // $("#addLevel").css("right","-100vw");
});
$("#addCountry .sidebar-title").on("click",function(){
        if(window.navigator.userAgent.indexOf("Trident")!=-1)
    {
        if($(window).width()<= 1074) 
            {
                $("#addFramework,#addLevel").css({
                    "-webkit-transform": "translateY(100%)",
                    "-ms-transform": "translateY(100%)",
                    "transform": "translateY(100%)"
                }); 
            } 
        else 
            {
                $("#addFramework,#addLevel").css({
                    "-webkit-transform": "translateX(100vw)",
                    "-ms-transform": "translateX(100vw)",
                    "transform": "translateX(100vw)"
                }); 
            }
    }
    else
    {
        if($(window).width()<= 1074) 
            {
                $("#addFramework,#addLevel").css({
                    "bottom": "-100%"
                }); 
            } 
        else 
            {
                $("#addFramework,#addLevel").css({
                    "right": "-100vw"
                }); 
            }  
    }
    // $("#addFramework").css("right","-100vw");
    // $("#addLevel").css("right","-100vw");
});
function clearError()
{
    $(".error").html("");
}


function ajaxGet(stage, url, header)
{
    let settings =
    {
        beforeSend: function()
        {
            $('.loader').css("display", "block");
        },
        complete: function()
        {
            $('.loader').removeClass("display").addClass("invisible");
        },
        "async": true,
        "crossDomain": true,
        "url": host + url,
        "method": "GET",
        "headers": header,
        success: function(data)
        {
            populateList(stage, data);
            $(".loader").removeClass("display").addClass("invisible");
            $(".siteContent").removeClass("invisible").addClass("display");
        },
        error: function(error)
        {
            console.log(error);
        }
    }
    $.ajax(settings);

}

function populateList(stage, data)
{
    let template = '';
    if (stage == "Country")
    {
        for (let i = 0; i < data.data.length; i++)
        {
            template += `<div class="list" id="` + stage + "-" + data.data[i].id + `">
                                    <div class="icon"><i class="far fa-check-circle"></i></div>
                                    <div class="listLabel">` + data.data[i].attributes.name + `</div>
                                </div>`;
        }
    }
    else if(stage=="Framework")
    {
        for (let i = 0; i < data.data.length; i++)
        {
            template += `<div class="list" id="` + stage + "-" + data.data[i].id + `">
                                  <div class="icon"><i class="far fa-check-circle"></i></div>
                                  <div class="listLabel">` + data.data[i].attributes.label + `</div>
                              </div>`;
        }
    }
    else
    {
              for (let i = 0; i < data.data.length; i++)
        {
            template += `<div class="list" id="` + stage + "-" + data.data[i].id + `">
                                <div class="icon"><i class="far fa-check-circle"></i></div>
                                  <div class="listLabel">` + data.data[i].attributes.label + `</div>
                                  <div class="rightbox"><div class="xp">
                                  <div class="label">Threshold XP&nbsp;</div><span class="value">`+data.data[i].attributes.threshold_xp+`</span></div></div>
                              </div>`;
        }
    }
    console.log($("#select" + stage).find(".form"));
    $("#select" + stage).find(".form").html(template);
}

$("body").on("click", ".list", function()
{
    $(this).parent().find(".icon").removeClass('selected');
    $(this).find(".icon").addClass('selected');
    if ($(this).attr("id").trim().split("-")[0] == "Country")
    {
        $("#selectFramework .title-bar").html("");
        $("#selectFramework .title-bar").html($(this).find(".listLabel").html());
        $("#selectFramework").find(".form").html("");
        currentStatus.currentCountry.id = $(this).attr("id").trim().split("-")[1];
        currentStatus.currentFramework.id = undefined;
        currentStatus.currentLevel.id = undefined;
        $("#selectFramework").find(".icon").removeClass('selected');
        $("#selectLevel").find(".icon").removeClass('selected');
        ajaxGet("Framework", "/api/v1/countries/" + currentStatus.currentCountry.id + "/frameworks", { Authorization: "Bearer " + token });
        
    }
    else if ($(this).attr("id").trim().split("-")[0] == "Framework")
    {
        $("#selectLevel .title-bar").html("");
        $("#selectLevel .title-bar").html($(this).find(".listLabel").html());
        $("#selectLevel").find(".form").html("");
        currentStatus.currentFramework.id = $(this).attr("id").trim().split("-")[1];
        currentStatus.currentLevel.id = undefined;
        $("#selectLevel").find(".icon").removeClass('selected');
        ajaxGet("Level", "/api/v1/frameworks/" + currentStatus.currentFramework.id + "/levels", { Authorization: "Bearer " + token });
        
    }
    else if ($(this).attr("id").trim().split("-")[0] == "Level", { Authorization: "Bearer " + token })
    {
        currentStatus.currentLevel.id = $(this).attr("id").trim().split("-")[1];
    }

});
$("#onBoardSubmit").on("click",function(){
    if(currentStatus.currentCountry.id==undefined || currentStatus.currentFramework.id==undefined || currentStatus.currentLevel.id==undefined)
    {
        Error.globalError("Incomplete Form.")
    }
    else
    {
        let settings =
        {
            "async": true,
            "crossDomain": true,
            "url": "https://kn-career-matrix.herokuapp.com/api/v1/transition",
            "method": "POST",
            "headers":
            {
                "Authorization": "Bearer " + token,
                "Content-Type": "application/json"
            },
            "data": JSON.stringify(
            {
                "meta": {
                    "transition_type": "switch",
                    "to_country": {
                        "type": "country",
                        "id": currentStatus.currentCountry.id
                    },
                    "to_framework": {
                        "type": "framework",
                        "id": currentStatus.currentFramework.id
                    },
                    "to_level": {
                        "type": "level",
                        "id": currentStatus.currentLevel.id
                    }
                }
            }
            ),
            success: function(data)
            {
                window.location.href = "./dashboard";

            },
            error: function(error)
            {
                console.log(error)
                Error.globalError(error.responseJSON.errors[0].detail)
            }
        }
        console.log(settings);
        $.ajax(settings);
    }
});

function browserWidth()
{
    if(window.navigator.userAgent.indexOf("Trident")!=-1 && $("body").width()>1074)
    {       
        // console.log(width)
        $(".list").width(width);
    }
    else
    {
        $(".sidebar").addClass('chrome-transition');
    }
}