import '../sass/sign_up.sass'
// import '../sass/template.sass'
import 'jquery'
import '../font-awesome/fontawesome.css'
// import '../sass/navbar_lip.sass'
import * as Error from './error'
// import '../sass/loader.sass'
// import '../sass/error.sass'
// import '../sass/media.sass'
// import '../sass/input.sass'

// import '../sass/sidebar.sass'
// import '../sass/scrollbar.sass'
var token;
var Heirarchy = ['Basic', 'Option'];
var host = "https://kn-career-matrix.herokuapp.com";
var currentTop = 'UserDetails';
var emp_id;
var currentStatus =
{
    currentBasic:
    {
        "name": undefined,
        "kn_code": undefined,
        "password": undefined,
        "password_confirmation": undefined
    },
    currentCountry:
    {
        type: "country",
        id: undefined
    },
    currentFramework:
    {
        type: "framework",
        id: undefined
    },
    currentLevel:
    {
        type: "level",
        id: undefined
    }
};
var inputFields =
{
    "Basic": [
    {
        "placeholder": "Full Name",
        "name": "Name",
        "type": "text"
    },
    {
        "placeholder": "Email Address",
        "name": "email",
        "type": "email"
    },
    {
        "placeholder": "KN Code",
        "name": "kNCode",
        "type": "text"
    },
    {
        "placeholder": "Password",
        "name": "password",
        "type": "password",
    },
    {
        "placeholder": "Confirm Password",
        "name": "password_confirmation",
        "type": "password"
    }],
    "Level": [
        {
            "placeholder": "Label",
            "name": "label",
            "type": "text"
        },
        {
            "placeholder": "Threshold XP",
            "name": "threshold_xp",
            "type": "number"
        }
    ],
    "UserDetails": [
    {
        "placeholder": "name"
    }],
    "Country": [{}]

};
var dataTypes = {
    "text": "string",
    "number": "integer",
    "checkbox": "boolean",
    "textarea": "string"
}

$("body").on("click", ".sidebar-title", function() {
    let order = currentTop;
    var destPane;
    var strSlice = $(this).html().trim();
    if (strSlice != currentTop) {
        var requiredId = "#add" + strSlice;
        for (let i = 3; i >= 0; i--) {
            destPane = '#add' + Heirarchy[i];
            if (requiredId == destPane) {
                currentTop = Heirarchy[i];
                break;
            } else {
                $(destPane).css({
                "-webkit-transform": "translateX(100vw)",
                "-ms-transform": "translateX(100vw)",
                "transform": "translateX(100vw)"
            });
            }
        }
    }
});

function comparePassword()
{
    if ($("#password").val().trim() == $("#password_confirmation").val().trim() && $("#password_confirmation").val().trim().length>=8)
    {
        return true
    }
    if($("#password_confirmation").val().length<8)
    {
        $("#error-password_confirmation").html("The password should be a minimum of 8 characters");
    }
    else
    {
        $("#error-password_confirmation").html("Password does not match");
    }
    return false
}

$("*").dblclick(function(e)
{
    e.preventDefault();
});
$("body").on("click", "#addNewOption", function()
{
    clearError();
    if (basicValidate() == true)
    {
        if (comparePassword() == true)
        {
            currentStatus.currentBasic.name = $("#name").val();
            currentStatus.currentBasic.kn_code = $("#kn_code").val();
            currentStatus.currentBasic.password = $("#password").val();
            currentStatus.currentBasic.password_confirmation = $("#password_confirmation").val();
            currentTop = 'Option';
            let settings =
            {
                "async": true,
                "crossDomain": true,
                "url": "https://kn-career-matrix.herokuapp.com/api/v1/employees/" + emp_id,
                "method": "PATCH",
                "headers":
                {
                    "Authorization": "Bearer " + token,
                    "Content-Type": "application/json"
                },
                "data": JSON.stringify(
                {
                    "data": {
                        "type": "employee",
                        "id": emp_id,
                        "attributes": currentStatus.currentBasic
                    }
                }),
                success: function(data)
                {
                    $("#addOption").css({
                "-webkit-transform": "translateX(-100vw)",
                "-ms-transform": "translateX(-100vw)",
                "transform": "translateX(-100vw)"
            });
                },
                error: function(error)
                {
                    console.log(error);
                }
            }
console.log(settings);
            $.ajax(settings);
        }
    }
});


//search algo for the search bar for country, framework and level form
$("#searchCountry , #searchFramework , #searchLevel").on("keyup ", function()
{
    let val = $(this).val();
    let listOfAllList = $(this).parent().parent().find(".list");
    if (val == "")
    {
        for (let i = 0; i < listOfAllList.length; i++)
        {
            listOfAllList[i].style.classList.remove("invisible");
            listOfAllList[i].style.className+= " display";
            
        }
    }
    else
    {
        for (let i = 0; i < listOfAllList.length; i++)
        {
            if (listOfAllList[i].children[0].innerHTML.trim().toLowerCase().indexOf(val.trim().toLowerCase()) > -1)
            {
                listOfAllList[i].style.classList.remove("invisible");
                listOfAllList[i].style.className+= " display";
            }
            else
            {
                listOfAllList[i].style.classList.remove("display");
                listOfAllList[i].style.className+= " invisible";
            }
        }
    }
});
$(document).ready(function()
{
    browserWidth();
    var urlObject={};
    let urlArray=location.href.split("?")[1].split("&")
    for(let i=0 ;i<urlArray.length;i++)
    {
        urlObject[urlArray[i].split("=")[0]]=urlArray[i].split("=")[1]
    }
    let settings =
    {
        "async": true,
        "crossDomain": true,
        "url": "https://kn-career-matrix.herokuapp.com/api/v1/employees/verify_token",
        "method": "POST",
        "headers":
        {
            "Content-Type": "application/json"
        },
        "data": JSON.stringify({ "data": { "id": urlObject["verify_email_token"]} }),
        success: function(data)
        {
            token = data.data.id;
            emp_id = data.included[0].id;
            ajaxGet("Country",  "/api/v1/countries",{"Content-Type": "application/json", "Authorization" : "Bearer " + token })
            $(".loader").removeClass("display").addClass("invisible");
            $(".siteContent").removeClass("invisible").addClass("display");
        },
        error: function(error)
        {
            window.location.href = "https://kn-career-matrix-front-end.herokuapp.com/?message=failure";
            console.log(error)
        }
    }
    $.ajax(settings);

});
$("body").find("#selectCountry").on("click", ".list", function()
{
    $("#selectFramework").removeClass("invisible").addClass("display");
    $(".sidebar").css("background-color", "white");
});

$("body").find("#selectFramework").on("click", ".list", function()
{
    $("#selectLevel").removeClass("invisible").addClass("display");
})

function basicValidate() {
    var listOfInputs = $("#addBasic").find("input");
    for (let i = 0; i < listOfInputs.length; i++)
    {
        if (listOfInputs[i].value == "")
        {
            listOfInputs[i].parentElement.nextElementSibling.innerHTML = "Empty Field"
            return false;
        }
    }
    return true;
}

function clearError()
{
    $(".error").html("");
}


function ajaxGet(stage, url, header)
{
    let settings =
    {
        beforeSend: function()
        {
            $('.loader').removeClass("invisible").addClass("display");
        },
        complete: function()
        {
            $('.loader').removeClass("display").addClass("invisible");
        },
        "async": true,
        "crossDomain": true,
        "url": host + url,
        "method": "GET",
        "headers": header,
        success: function(data)
        {
            populateList(stage, data);
        },
        error: function(error)
        {
            console.log(error);
        }
    }
    $.ajax(settings);

}

function populateList(stage, data)
{
    let template = '';
    if (stage == "Country")
    {
        for (let i = 0; i < data.data.length; i++)
        {
            template += `<div class="list" id="` + stage + "-" + data.data[i].id + `">
                                    <div class="listLabel">` + data.data[i].attributes.name + `</div>
                                    <div class="icon"><i class="fas fa-check"></i></div>
                                </div>`;
        }
    }
    else
    {
        for (let i = 0; i < data.data.length; i++)
        {
            template += `<div class="list" id="` + stage + "-" + data.data[i].id + `">
                                  <div class="listLabel">` + data.data[i].attributes.label + `</div>
                                  <div class="icon"><i class="fas fa-check"></i></div>
                              </div>`;

        }
    }
        console.log($("#select" + stage).find(".form"))
    $("#select" + stage).find(".form").html(template);
}

$("body").on("click", ".list", function()
{
    $(this).parent().find(".icon").removeClass('selected');
    $(this).find(".icon").addClass('selected');
    if ($(this).attr("id").trim().split("-")[0] == "Country")
    {
        $("#selectFramework , #selectLevel").find(".form").html("");
        currentStatus.currentCountry.id = $(this).attr("id").trim().split("-")[1];
        currentStatus.currentFramework.id = undefined;

        ajaxGet("Framework", "/api/v1/countries/" + currentStatus.currentCountry.id + "/frameworks", { Authorization: "Bearer " + token });
        $("#selectFramework").find(".icon").removeClass('selected');
        $("#selectLevel").find(".icon").removeClass('selected');
    }
    else if ($(this).attr("id").trim().split("-")[0] == "Framework")
    {
        $("#selectLevel").find(".form").html("");
        currentStatus.currentFramework.id = $(this).attr("id").trim().split("-")[1];
        currentStatus.currentLevel.id = undefined;
        $("#selectLevel").find(".icon").removeClass('selected');
        ajaxGet("Level", "/api/v1/frameworks/" + currentStatus.currentFramework.id + "/levels", { Authorization: "Bearer " + token });
    }
    else if ($(this).attr("id").trim().split("-")[0] == "Level", { Authorization: "Bearer " + token })
    {
        currentStatus.currentLevel.id = $(this).attr("id").trim().split("-")[1];
    }

});
$("#onBoardSubmit").on("click",function(){
    if(currentStatus.currentCountry.id==undefined || currentStatus.currentFramework.id==undefined || currentStatus.currentLevel.id==undefined)
    {
        Error.globalError("Incomplete Form.")
    }
    else
    {
        let settings =
        {
            "async": true,
            "crossDomain": true,
            "url": "https://kn-career-matrix.herokuapp.com/api/v1/transition",
            "method": "POST",
            "headers":
            {
                "Authorization": "Bearer " + token,
                "Content-Type": "application/json"
            },
            "data": JSON.stringify(
            
                {
                    "meta": {
                        "transition_type": "onboard",
                        "to_country": {
                            "type": "country",
                            "id": currentStatus.currentCountry.id
                        },
                        "to_framework": {
                            "type": "framework",
                            "id": currentStatus.currentFramework.id
                        },
                        "to_level": {
                            "type": "level",
                            "id": currentStatus.currentLevel.id
                        }
                    }
                }
            ),
            success: function(data)
            {
                window.location.href = "https://kn-career-matrix-front-end.herokuapp.com/?token="+token;

            },
            error: function(error)
            {
                Error.globalError(error.errors["0"].detail)
            }
        }
        console.log(settings);
        $.ajax(settings);
    }

});
function browserWidth()
{
    if(window.navigator.userAgent.indexOf("Trident")!=-1 && $("body").width()>1074)
    {
        // $(".list").width(800);
        
        // console.log(width)
        // $(".list").width(width);
    }
    else
    {
        $(".sidebar").addClass('chrome-transition');
    }
}