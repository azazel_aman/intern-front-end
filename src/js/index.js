import'jquery'
import '../sass/index.sass'
// import '../sass/template.sass'
import '../font-awesome/fontawesome.css';
import * as Error from './error'
// import '../sass/navbar_lip.sass'
// import '../sass/loader.sass'
// import '../sass/error.sass'
// import '../sass/media.sass'
// import '../sass/input.sass'

// import '../sass/sidebar.sass'
// import '../sass/scrollbar.sass'
var token;
var emp_id;
function validateEmail(emailString)
{
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    if(re.test(emailString) == true)
    {
        return true;
    }
    else
    {
        return false;
    }
}
function clearErrorFields()
{
    $(".login-screen .emailValidate").html("");
    $(".login-screen .error").html("");
    $(".login-screen .passwordValidate").html("");
}
function clearField()
{
    clearErrorFields();
    $(".login-screen .signin-email").val("");
    $(".login-screen .signin-password").val("");
    $(".login-screen .forgot-email").val("")
    $(".login-screen .emailValidate").html("")
    $(".login-screen .error").html("");
    $(".login-screen .passwordValidate").html("");
}

function showModal()
{
    clearField();
    $(".login-screen .title").html("Forgot Password");
    $(".login-screen .passwordValidate").removeClass("display").addClass("invisible");
    $(".login-screen #passwordDiv").removeClass("display").addClass("invisible");
    $(".login-screen input").first().removeClass('signin-email').removeClass("new-email").addClass("forgot-email");
    $(".login-screen .forgotLink").removeClass("display").addClass("invisible");
    $(".login-screen .back").removeClass("invisible").addClass("display");
    $(".login-screen button").attr("id","submit-forgot");
    $(".login-screen .newLink").removeClass("display").addClass("invisible");
    $(".login-screen button").html("Submit");
    $("i").css("visibility","hidden");
}
function hideModal()
{

    clearField();
    $(".login-screen .title").html("Login");
    $(".login-screen .passwordValidate").removeClass("invisible").addClass("display");
    $(".login-screen #passwordDiv").removeClass("invisible").addClass("display");
    $(".login-screen input:eq(0)").addClass('signin-email').removeClass("forgot-email").removeClass("new-email").attr("placeholder","Email").attr('type', 'text');
    $(".login-screen input:eq(1)").addClass('signin-password').removeClass("forgot-confirm-password").removeClass("new-email").attr("placeholder","Password").attr('type', 'password');
    $(".login-screen .forgotLink").removeClass("invisible").addClass("display");
    $(".login-screen .back").removeClass("display").addClass("invisible");
    $(".login-screen .newLink").removeClass("invisible").addClass("display");
    $(".login-screen button").attr("id","submit-signin");
    $(".login-screen button").html("Login");
    $("i").css("visibility","visible");
}

function showNew()
{
    clearField();
    $(".login-screen .title").html("Sign Up");
    $(".login-screen .passwordValidate").removeClass("display").addClass("invisible");
    $(".login-screen #passwordDiv").removeClass("display").addClass("invisible");
    $(".login-screen input").first().removeClass('signin-email').removeClass('forgot-email').addClass("new-email");
    $(".login-screen .forgotLink").removeClass("display").addClass("invisible");
    $(".login-screen .newLink").removeClass("display").addClass("invisible");
    $(".login-screen .back").removeClass("invisible").addClass("display");
    $(".login-screen button").attr("id","submit-new");
    $(".login-screen button").html("Submit");
    $("i").css("visibility","hidden");
}
function submitSignin()
{

    var userMail = $(".signin-email").val();
    var password = $(".signin-password").val();
    clearErrorFields();
    if (validateEmail(userMail) && password.length > 5)
    {
        $.ajax(
        {
            type: 'POST',
            url: 'https://kn-career-matrix.herokuapp.com/api/v1/tokens',
            headers: {
                "Authorization": "Basic " + btoa(userMail + ":" + password)
                     },
            success: function(data)
                    {
                        console.log(data.data.id);
                        console.log(data)
                        localStorage.setItem("flag", "0");
                        localStorage.setItem("token", data.data.id);
                        localStorage.setItem("emp_id", data.included[0].id);
                        window.location.href="./dashboard"

                    },
            error: function(responseJSON)
                    {
                        console.log(responseJSON);
                        $(".login-screen .passwordValidate").html(responseJSON.statusText);
                    },
        });
    }
    else
    {
        if (password.length > 5 && validateEmail(userMail) != true)
        {
            $(".login-screen .emailValidate").html("Invalid Email").css("color","red");
        }
        else if (password.length < 5 && validateEmail(userMail) == true)
        {
            $(".login-screen .passwordValidate").html("Password length should be more than 8");
        }
        else
        {
            $(".login-screen .emailValidate").html("Invalid Email");
            $(".login-screen .passwordValidate").html("Password length should be more than 8");
        }
    }
}

function submitForgot()
{
    var forgotMail = $(".forgot-email").val();
    if (validateEmail(forgotMail) != true)
    {
        $(".login-screen .emailValidate").css("color", "RED");
        $(".login-screen .emailValidate").html("Invalid Mail");
    }
    else
    {
        let settings={
            "async": true,
            "crossDomain": true,
            type: 'POST',
            "headers":{
                        "Content-Type": "application/json"
                      },
            url: 'https://kn-career-matrix.herokuapp.com/api/v1/employees/reset_password',
            data: JSON.stringify({
                                   "data": {
                                            "type": "employee",
                                            "attributes": {
                                                            "email": forgotMail
                                                          }
                                            }
                                }),
            success: function(data)
                    {
                        Error.globalsuccess("A recovery mail has been send to your inbox. Kindly refer it.")
                    },
            error: function(responseJSON)
                    {
                      console.log(responseJSON)
                      Error.globalError(responseJSON.responseJSON.errors["0"].detail)
                    },
        }
        console.log(settings);
        $.ajax(settings);
    }
}

function submitNew()
{
    var newMail = $(".new-email").val();
    console.log(newMail);
    if (validateEmail(newMail) != true)
    {
        $(".login-screen .emailValidate").css("color", "RED");
        $(".login-screen .emailValidate").html("Invalid Mail");
    }
    else
    {

        var settings = {
          "async": true,
          "crossDomain": true,
          "url": "https://kn-career-matrix.herokuapp.com/api/v1/employees/verify_email",
          "method": "POST",
          "headers":{
            "Content-Type": "application/json"
          },
          "data": JSON.stringify({"data" : {"type" : "employee", "attributes" : {"email" : newMail} } }), success: function()
                     {
                         Error.globalsuccess("A confirmation mail has been sent to your inbox");
                     },
             error: function(error)
                     {
                         console.log(error)
                         Error.globalError(error.responseJSON.errors["0"].detail)
                     }
        }
        $.ajax(settings);
    }
}

$(document).ajaxStart(function()
{
    $(".loader").removeClass("invisible").addClass("display-loader");
});

$(document).ajaxStop(function()
{
    $(".loader").removeClass("display-loader").addClass("invisible");
});
$(document).ready(function()
{
    // $(".loader").removeClass("display").addClass("invisible");
    var urlObject={};
if($("body").width()<=1074)
{
    $("#showNew , i").remove();
}
    if(window.location.href.indexOf("?")>-1)
    {
        let urlArray=location.href.split("?")[1].split("&");
        for(let i=0 ;i<urlArray.length;i++)
        {
            urlObject[urlArray[i].split("=")[0]]=urlArray[i].split("=")[1]
        }
        console.log(urlObject);
        if("message" in urlObject)
        {
            $(".mainScreen").removeClass('invisible').addClass('display');
            $(".loader").removeClass("display-loader").addClass("invisible");
            if(urlObject["message"]=="failure")
            {
                Error.globalError("The link has expired. Kindly Sign Up again.")
            }
            else if(urlObject["message"] == "successPassword")
            {
                Error.globalsuccess("The password is changed successfully");
            }
        }
        else if("reset_password_token" in urlObject)
        {
            let settings =
            {
                "async": true,
                "crossDomain": true,
                "url": "https://kn-career-matrix.herokuapp.com/api/v1/employees/verify_token",
                "method": "POST",
                "headers":
                {
                    "Content-Type": "application/json"
                },
                "data": JSON.stringify({"data": {"type": "reset_password_token","id": urlObject.reset_password_token}}),
                success: function(data)
                {
                    token = data.data.id;
                    emp_id = data.included[0].id;
                    $(".mainScreen").addClass('display');
                    $(".loader").removeClass("display-loader").addClass("invisible");
                },
                error: function(error)
                {
                    window.location.href = "./?message=failure";
                    console.log(error)
                }
            }
            console.log()
            $.ajax(settings);
            $("i").css("visibility","hidden");
            $(".login-screen .title").html("Reset Password");
            $("input.signin-email").first().removeClass('signin-email').removeClass("new-email").addClass("forgot-password").attr("type","password").attr('placeholder', 'Password');
            $("input.signin-password").removeClass('signin-password').addClass('forgot-confirm-password').attr('placeholder', 'Confirm Password');
            $(".forgot-password").parent().find("span").html("New password")
            $(".login-screen .forgotLink").removeClass("display").addClass("invisible");
            $(".login-screen .back").removeClass("invisible").addClass("display");
            $(".login-screen button").attr("id","submit-forgot-password");
            $(".login-screen .newLink").removeClass("display").addClass("invisible");
            $(".login-screen button").html("Reset");
        }
        else if("token" in urlObject)
        {
            $(".mainScreen").removeClass('invisible').addClass('display');
            $(".loader").removeClass("display-loader").addClass("invisible");
        }


    }
    else
    {
        $(".mainScreen").removeClass('invisible').addClass('display');
        $(".loader").removeClass("display-loader").addClass("invisible");
    }

});

$("body").on("click","#showModal",function(){
  showModal();
});
$("body").on("click","#hideModal",function(){
  hideModal();
});
$("body").on("click","#showNew",function(){
  showNew();
});
$("body").on('click',"#submit-signin", function() {
  submitSignin();
});
$("body").on('click',"#submit-forgot",function() {
  submitForgot();
});
$("body").on('click', "#submit-new",function() {
  submitNew();
});

$("body").on("click","#submit-forgot-password",function(){
    console.log("aman")
    if(document.getElementsByTagName("input")[0].value==document.getElementsByTagName("input")[1].value && document.getElementsByTagName("input")[0].value.length>=8)
    {
           let settings =
            {
                "async": true,
                "crossDomain": true,
                "url": "https://kn-career-matrix.herokuapp.com/api/v1/employees/" + emp_id,
                "method": "PATCH",
                "headers":
                {
                    "Authorization": "Bearer " + token,
                    "Content-Type": "application/json"
                },
                "data": JSON.stringify(
                {
                    "data": {
                        "type": "employee",
                        "id": emp_id,
                        "attributes": {"password": document.getElementsByTagName("input")[0].value, "password_confirmation": document.getElementsByTagName("input")[1].value}
                    }
                }),
                success: function(data)
                {
                    console.log(data);
                    window.location.href = "./?message=successPassword";
                },
                error: function(error)
                {
                    Error.globalError(error.errors["0"].detail)
                    console.log(error);
                }
            }
            $.ajax(settings);
    }
    else if(document.getElementsByTagName("input")[0].value==document.getElementsByTagName("input")[1].value && document.getElementsByTagName("input")[0].value.length<8)
    {
        $(".passwordValidate").html("The password should contain atleast 8 characters.")   
    }
    else
    {
        $(".passwordValidate").html("The password doesnot match.")
    }
});
$("input").keypress(function(e){
if(e.keyCode==13)
{
    e.preventDefault();
    console.log("aman")
    document.getElementsByTagName("button")[0].click();
}
})