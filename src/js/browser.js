// jQBrowser v0.2: http://davecardwell.co.uk/javascript/jquery/plugins/jquery-browserdetect/
eval(function(p, a, c, k, e, d) 
	{ e = function(c) 
		{ 
			return (c < a ? "" : e(c / a)) + String.fromCharCode(c % a + 161) 
		}; 
		while (c--) 
			{ 
				if (k[c]) 
					{ 
						p = p.replace(new RegExp(e(c), 'g'), k[c]) 
					} 
			} return p 
	});
/* ----------------------------------------------------------------- */

var aol = $.browser.aol(); // AOL Explorer
var camino = $.browser.camino(); // Camino
var firefox = $.browser.firefox(); // Firefox
var flock = $.browser.flock(); // Flock
var icab = $.browser.icab(); // iCab
var konqueror = $.browser.konqueror(); // Konqueror
var mozilla = $.browser.mozilla(); // Mozilla
var msie = $.browser.msie(); // Internet Explorer Win / Mac
var netscape = $.browser.netscape(); // Netscape
var opera = $.browser.opera(); // Opera
var safari = $.browser.safari(); // Safari

var userbrowser = $.browser.browser(); //detected user browser

//operating systems

var linux = $.browser.linux(); // Linux
var mac = $.browser.mac(); // Mac OS
var win = $.browser.win(); // Microsoft Windows

//version

var userversion = $.browser.version.number();
let answer = {
				check_os: {
								"linux": linux,
								"mac": mac,
								"win": win
				},
				check_browser: {
								"firefox": firefox,
								"mozilla": mozilla,
								"msii": msii,
								"safari": safari
				}

}
export { answer }
